import 'dart:convert';

import 'package:clickable_regions/pages/values/mapValues.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import '../map_svg_data.dart';
import '../map_svg_data.dart';
import '../map_svg_data.dart';
import '../models/CountryModel.dart';
import '../pages/values/mapValues.dart' as mapValues;
import 'package:http/http.dart' as http;

import '../province.dart';

class WorldMap extends StatefulWidget {
  final ValueChanged<void> refresh;

  const WorldMap({Key key, this.refresh}) : super(key: key);
  @override
  _WorldMapState createState() => _WorldMapState();
}

class _WorldMapState extends State<WorldMap> {
  var futureGetData;

  @override
  void initState() {
    super.initState();
    futureGetData = getData();
  }

  initMapValues(BuildContext context) {
    mapValues.scaleFactor = 0.5;
    setState(() {});
  }

  void _provincePressed(Province province) {
    setState(() {
      mapValues.pressedProvince = province;
    });
  }

  getData() async {
    print("start get data");
    initMapValues(context);

    var res =
        await http.get(Uri.encodeFull("http://sobhanm28.ir/statistics.json"));
    if (res.statusCode == 200) {
      mapData = json.decode(res.body);
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: futureGetData,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          if (snapshot.data) {
            return SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Stack(
                    children: List.generate(Province.values.length, (i) {
                  String co =
                            Province.values[i].toString().split(".").last;

                  return GestureDetector(
                      onTap: () {
                        _provincePressed(Province.values[i]);
                      

                        if (mapData[co] != null) {
                          country = mapData[co]["country"];
                          infected = mapData[co]["infected"];
                          death = mapData[co]["deaths"];
                          lastUpdate = mapData[co]["history"].last["timestamp"];

                          print(country);
                        } else {
                          country = "";
                        }
                        widget.refresh(null);
                      },
                      child: CountryModel(
                        province: Province.values[i],
                        death:mapData[co]!=null? mapData[co]["deaths"]:0,
                        infected:mapData[co]!=null? mapData[co]["infected"]:0,

                      ));
                })),
              ),
            );
          }
        } else if (snapshot.hasError) {
          print("snap Shot ERR : ${snapshot.error}");
          return Container();
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}

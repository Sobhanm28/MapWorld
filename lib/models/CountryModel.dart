import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../pages/values/mapValues.dart' as mapValues;
import '../map_svg_data.dart';
import '../province.dart';

class CountryModel extends StatefulWidget {
  final Province province;
  final int infected;
  final int death;

  const CountryModel({Key key, this.province, this.infected, this.death})
      : super(key: key);
  @override
  _CountryModelState createState() => _CountryModelState();
}

class _CountryModelState extends State<CountryModel> {
  int maxInfected = 0;
  double alpha = 0;
  Color color = Colors.blue[200];
  @override
  void initState() {
    super.initState();
    calculateAlpha();
  }

  calculateAlpha() {
    mapValues.globalInfected.add(widget.infected);
    mapValues.globalInfected.sort();
    maxInfected = mapValues.globalInfected.last;
    print(mapValues.globalInfected);
    alpha = widget.infected / maxInfected;
    if (alpha == 0) {
      setState(() {
        color = Colors.green[300];
      });
    } else if (alpha < 0.2) {
      setState(() {
        color = Colors.red[100];
      });
    } else if (alpha > 0.2 && alpha < 0.4) {
      setState(() {
        color = Colors.red[400];
      });
    } else if (alpha > 0.4 && alpha < 0.6) {
      setState(() {
        color = Colors.red[600];
      });
    } else if (alpha > 0.6 && alpha < 0.8) {
      setState(() {
        color = Colors.red[800];
      });
    } else if (alpha > 0.8 && alpha <= 1) {
      setState(() {
        color = Colors.black;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return ClipPath(
        child: Stack(children: <Widget>[
          CustomPaint(painter: PathPainter(widget.province)),
          Material(
              color: Colors.transparent,
              child: Container(
                  width: 1000,
                  height: 804,
                  color: mapValues.pressedProvince == widget.province
                      ? Color(0xFF7C7C7C)
                      : color))
        ]),
        clipper: PathClipper(widget.province));
  }
}

class PathPainter extends CustomPainter {
  final Province _province;
  PathPainter(this._province);

  @override
  void paint(Canvas canvas, Size size) {
    Path path = getPathByProvince(_province);
    canvas.drawPath(
        path,
        Paint()
          ..style = PaintingStyle.stroke
          ..color = Colors.black
          ..strokeWidth = 1.0);
  }

  @override
  bool shouldRepaint(PathPainter oldDelegate) => true;

  @override
  bool shouldRebuildSemantics(PathPainter oldDelegate) => false;
}

class PathClipper extends CustomClipper<Path> {
  final Province _province;
  PathClipper(this._province);

  @override
  Path getClip(Size size) {
    return getPathByProvince(_province);
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}

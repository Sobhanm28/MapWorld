import 'package:clickable_regions/pages/values/mapValues.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class DetailBox extends StatefulWidget {
  @override
  _DetailBoxState createState() => _DetailBoxState();
}

class _DetailBoxState extends State<DetailBox> {
  width(double d) {
    return MediaQuery.of(context).size.width * d;
  }

  height(double d) {
    return MediaQuery.of(context).size.height * d;
  }

  @override
  Widget build(BuildContext context) {
    return country==""?Container(): Container(
      width: width(1),
      height: height(0.2),
      color: Colors.blue[300],
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Text(
            "$country",
            style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Text(
                "فوتی ها :$death",
                style: TextStyle(fontSize: 18),
              ),
              Text(
                "مبتلایان : $infected",
                style: TextStyle(fontSize: 18),
              ),
            ],
          ),
          Text("$lastUpdate")
        ],
      ),
    );
  }
}

import 'package:clickable_regions/pages/values/mapValues.dart';
import 'package:clickable_regions/widgets/DetailBox.dart';
import 'package:clickable_regions/widgets/WorldMap.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  width(double d) {
    return MediaQuery.of(context).size.width * d;
  }

  height(double d) {
    return MediaQuery.of(context).size.height * d;
  }

  refresh(_) {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    /// Calculate the center point of the SVG map,
    /// use parent widget for width/heigth.
    return Scaffold(
        body: Stack(
      children: <Widget>[
        Container(
            margin: EdgeInsets.only(top: height(0.1)),
            child: WorldMap(
              refresh: refresh,
            )),
        Positioned(bottom: 0, child: DetailBox())
      ],
    ));
  }
}

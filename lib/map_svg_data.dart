///
/// Created by Giovanni Terlingen
/// See LICENSE file for more information.
///
import 'dart:ui';

import 'package:built_path_annotations/built_path_annotations.dart';

import 'province.dart';
part 'map_svg_data.svg_path.g.dart';

Path getPathByProvince(Province province) {
  switch (province) {

    case Province.DZ:
      return MapSvgData.dz;
    case Province.AZ:
      return MapSvgData.az;

    case Province.AM:
      return MapSvgData.am;
    case Province.AO:
      return MapSvgData.ao;

    case Province.AR:
      return MapSvgData.ar;
    case Province.AU:
      return MapSvgData.au;

    case Province.BS:
      return MapSvgData.bs;
    case Province.BD:
      return MapSvgData.bd;
    case Province.BZ:
      return MapSvgData.bz;

    case Province.BO:
      return MapSvgData.bo;
    case Province.MM:
      return MapSvgData.mm;
    case Province.BJ:
      return MapSvgData.bj;
    case Province.SB:
      return MapSvgData.sb;
    case Province.BR:
      return MapSvgData.br;
    case Province.BG:
      return MapSvgData.bg;

    case Province.KH:
      return MapSvgData.kh;
    case Province.LK:
      return MapSvgData.lk;
    case Province.CG:
      return MapSvgData.cg;
    case Province.CD:
      return MapSvgData.cd;

    case Province.CN:
      return MapSvgData.cn;
    case Province.AF:
      return MapSvgData.af;

    case Province.CL:
      return MapSvgData.cl;

    case Province.CM:
      return MapSvgData.cm;
    case Province.TD:
      return MapSvgData.td;

    case Province.CO:
      return MapSvgData.co;
    case Province.CR:
      return MapSvgData.cr;
    case Province.CF:
      return MapSvgData.cf;
    case Province.CU:
      return MapSvgData.cu;
    case Province.CV:
      return MapSvgData.cv;
    case Province.CK:
      return MapSvgData.ck;

    case Province.DK:
      return MapSvgData.dk;

    case Province.EC:
      return MapSvgData.ec;
    case Province.DO:
      return MapSvgData._do;
    case Province.EG:
      return MapSvgData.eg;
    case Province.IE:
      return MapSvgData.ie;

    case Province.EE:
      return MapSvgData.ee;
    case Province.ER:
      return MapSvgData.er;

    case Province.ET:
      return MapSvgData.et;
    case Province.AT:
      return MapSvgData.at;
    case Province.CZ:
      return MapSvgData.cz;

    case Province.FI:
      return MapSvgData.fi;
    case Province.FJ:
      return MapSvgData.fj;
      //==================11111111111111111====================
    case Province.FK:
      return MapSvgData.fk;
    case Province.FM:
      return MapSvgData.fm;
    case Province.PF:
      return MapSvgData.pf;
    case Province.FR:
      return MapSvgData.fr;
   
    case Province.GA:
      return MapSvgData.ga;
    case Province.GE:
      return MapSvgData.ge;
    case Province.GH:
      return MapSvgData.gh;

    case Province.GL:
      return MapSvgData.gl;
    case Province.DE:
      return MapSvgData.de;
 
    case Province.GR:
      return MapSvgData.gr;
  
    case Province.GN:
      return MapSvgData.gn;
    case Province.GY:
      return MapSvgData.gy;
    case Province.HT:
      return MapSvgData.ht;
    case Province.HN:
      return MapSvgData.hn;
    case Province.HR:
      return MapSvgData.hr;
    case Province.HU:
      return MapSvgData.hu;
    case Province.IS:
      return MapSvgData._is;
    case Province.IN:
      return MapSvgData._in;
    case Province.IR:
      return MapSvgData.ir;

    case Province.IT:
      return MapSvgData.it;
    case Province.CI:
      return MapSvgData.ci;
    case Province.IQ:
      return MapSvgData.iq;
    case Province.JP:
      return MapSvgData.jp;
  
    case Province.KE:
      return MapSvgData.ke;
    case Province.KG:
      return MapSvgData.kg;
    case Province.KP:
      return MapSvgData.kp;
    case Province.KI:
      return MapSvgData.ki;
    case Province.KR:
      return MapSvgData.kr;
 
    case Province.KZ:
      return MapSvgData.kz;
    case Province.LA:
      return MapSvgData.la;

    case Province.BY:
      return MapSvgData.by;

    case Province.LY:
      return MapSvgData.ly;
    case Province.MG:
      return MapSvgData.mg;

    case Province.MN:
      return MapSvgData.mn;

    case Province.ML:
      return MapSvgData.ml;
    case Province.MA:
      return MapSvgData.ma;

    case Province.MR:
      return MapSvgData.mr;

    case Province.OM:
      return MapSvgData.om;
    case Province.MV:
      return MapSvgData.mv;
    case Province.MX:
      return MapSvgData.mx;
    case Province.MY:
      return MapSvgData.my;
    case Province.MZ:
      return MapSvgData.mz;
    case Province.MW:
      return MapSvgData.mw;
    case Province.NC:
      return MapSvgData.nc;

    case Province.NE:
      return MapSvgData.ne;

    case Province.MP:
      return MapSvgData.mp;
    case Province.TF:
      return MapSvgData.tf;
   
    case Province.VU:
      return MapSvgData.vu;
    case Province.NG:
      return MapSvgData.ng;
    case Province.NL:
      return MapSvgData.nl;
    case Province.NO:
      return MapSvgData.no;
    case Province.NP:
      return MapSvgData.np;

    case Province.NI:
      return MapSvgData.ni;
    case Province.NZ:
      return MapSvgData.nz;

    case Province.PE:
      return MapSvgData.pe;
    case Province.PK:
      return MapSvgData.pk;
    case Province.PL:
      return MapSvgData.pl;
    case Province.PA:
      return MapSvgData.pa;
    case Province.PT:
      return MapSvgData.pt;
    case Province.PG:
      return MapSvgData.pg;

    case Province.MD:
      return MapSvgData.md;
    case Province.PH:
      return MapSvgData.ph;

    case Province.RU:
      return MapSvgData.ru;

    case Province.SA:
      return MapSvgData.sa;

    case Province.LS:
      return MapSvgData.ls;
    case Province.BW:
      return MapSvgData.bw;

    case Province.SG:
      return MapSvgData.sg;
    case Province.SO:
      return MapSvgData.so;

    case Province.LC:
      return MapSvgData.lc;
    case Province.SD:
      return MapSvgData.sd;
    case Province.SS:
      return MapSvgData.ss;
 
    case Province.TT:
      return MapSvgData.tt;
    case Province.TH:
      return MapSvgData.th;

    case Province.TK:
      return MapSvgData.tk;


    case Province.ST:
      return MapSvgData.st;
    case Province.TN:
      return MapSvgData.tn;
    case Province.TR:
      return MapSvgData.tr;

    case Province.TM:
      return MapSvgData.tm;
    case Province.TZ:
      return MapSvgData.tz;

    case Province.GB:
      return MapSvgData.gb;
    case Province.UA:
      return MapSvgData.ua;
    case Province.US:
      return MapSvgData.us;
    case Province.BF:
      return MapSvgData.bf;

    case Province.UZ:
      return MapSvgData.uz;

    case Province.VE:
      return MapSvgData.ve;

    case Province.VN:
      return MapSvgData.vn;

    case Province.NA:
      return MapSvgData.na;

    case Province.YE:
      return MapSvgData.ye;
    case Province.ZM:
      return MapSvgData.zm;
    case Province.ZW:
      return MapSvgData.zw;
    case Province.ID:
      return MapSvgData.id;

    case Province.AE:
      return MapSvgData.ae;

    case Province.MH:
      return MapSvgData.mh;

    case Province.EH:
      return MapSvgData.eh;
    case Province.RS:
      return MapSvgData.rs;

    case Province.SJ:
      return MapSvgData.sj;


    case Province.GS:
      return MapSvgData.gs;
 
  }
  return Path();
}

/// We can open our exported SVG image, then we put all SVG data we have in the
/// property it belongs to. Our build runner will create Paths for us to use
/// within the app. Run command: flutter packages pub run build_runner build
class MapSvgData {
  /// Height and width of the used SVG image
  static double get height => 504;
  static double get width => 1000;

 
  @SvgPath('m507 130-5 1-9 5 1 1 1 5 1 2v1h-5l-2 2v2l-3 1-2 2-4 1-4 3v3 2l5 3 5 4 16 12v1l2 2 4 1v3l3-1 4-1 4-4 13-8-1-3-4-1-2-5 1-1v-4l-1-5-1-2 1-1-2-6-2-1-2-4 2-3-1-6 2-1-6-1-3 2-1-1h-5z')
  static Path get dz => _$MapSvgData_dz;
  @SvgPath('m615 121v1l4 2-2-2-2-1zm1-3zm-1-1zm0 0h-1 1zm3-3-1 1 2 1-1 1-3-1h-1 1l3 3-1 1 2 1 1 3 3-3 1 1v2l2 1 1-5 2-1-3-1-3-4-2 2-3-2z')
  static Path get az => _$MapSvgData_az;

  @SvgPath('m615 116h-1l-4 1 1 1 1 2h1l4 2 2 2h1l-1-3-2-1 1-1-3-3zm0 1h-1 1zm0 0zm1 1z')
  static Path get am => _$MapSvgData_am;
  @SvgPath('m531 300h1l-1-1v1zm7-34h-2-3l3 8-1 2 2 5v3l-3 6-3 7 1 1v1l-1 3 4-1 3 1h12l1 2h6l6-1h1l-4-5v-10h6v-6l-5 1v-3l-1-3v-7h-4l1-1h-3l-1 3-5 1-2-3-1-4h-7zm-2-4-1-1-2 2v2h1v-2l2-1z')
  static Path get ao => _$MapSvgData_ao;

  @SvgPath('m338 420v-1 1zm12-1-3 1h1l2-1zm-13-5-1-1 2 6 6 1h2v-1l-6-2-4-2 1-1zm2-42v-1h-1l1 1zm-1-1zm-19-54-2-1-2 3 1 1-1 3-3 3 1 1v4l1 1-1 1-2 4-1 3 1 3-1 3 3 6 1 1v3l-1 3 1 3-1 2 2 5-1 2v2 3l2 5h-1l1 2 1 1 1 4h2v1h-2l2 1v1l1 4v1l-1 1v1l1 2v1l-1 2 2 4h2l1 3 2 1h4l5 1-3-2h-1 1l-1-2v-2l1-1h-2 3v-3l3-3-1-1h1l-1-2h-3l-3-3 1-3 3-1 1-2-1-2 2-2-2-1h1l1 1 1-1-1-1-1 1-2-1-1-4 4 1 3-1v-1l-1-1v-3h1l-2-2 4 1 6-2 2-1 2-4-1-2-2-1v-2l-4-2v-2-2-1-2-5-2l4-6 5-4v-5h-3l1 3-3 3h-8l2-6v-1l-10-4-5-5v-1h-3l-1 3-1-3h-2-1z')
  static Path get ar => _$MapSvgData_ar;
  @SvgPath('m871 419 1-1-1 1zm1-34v-1 1zm1-1zm3-2-1 1 1-1zm1-1zm-5-4-1-1-1 2v3h-1l-1 2 2 1-1 1h2l2-2 1 1 1-1 2-3v1l3-4-5 1-3-1zm0-1zm8 0zm-9 0 1-1-1 1zm1-1zm9 0h-1 1zm0 0 1-1-1-1v2zm-11-1h1v-1l-1 1zm10-2zm0 0zm0-2h-1 1zm-3-1h-1 1zm0 0zm-16-9-3 1h4l-1-1zm-2-1v-1 1zm-1-1zm-46 0zm50-1v-1 1zm-39-2zm2 0zm31-1zm-1-5h1-1zm-47 0h-1 1zm116-2zm-10-12 1-1h-1v1zm1-2v1-1zm-108-3-1-2v1l1 1zm108-3zm-108 0zm0-1zm108 3 2-4-2 3v1zm-3-6v-1 1zm0-1v-1 1zm0-1v-1 1zm-3-3zm2 0v-1 1zm-1 0v-1 1zm1-2zm0 0zm-95-3zm93-1h-1 1zm-1-1h1-1zm0 0h1-1zm-1 0v-1 1zm-4-3zm-1-2v-1 1zm-17-5h-1 1zm-2 0 1-1h-1v1zm2-2h-1v1l1-1zm-8-2h1-1zm1 0zm0 0v-1 1zm-1 0v-1 1zm1 0v-1 1zm-34-1v-1 1zm0 0v-1l-1 1h1zm1-1zm9 0zm-9 0v-1 1zm10 0v-1 1zm20-1zm-17 0zm0 0v1-1zm-12-1zm4-2zm28 0-1 1h2-1l1-1h-1zm-1 0v-1 1zm0-1v-1 1zm-10-4v-1 1zm12 0 1-1h-1v1zm-5-1zm0 0zm4 0zm-3 0zm4 0v-1 1zm0-1zm-2 0h1-1zm-9 0zm2 0zm9 0v-1l-1 1h1zm-8 0-1-1v1h1zm-9 0v-1l-1 1h1zm0-1v-1l1 3 2-2h-1-2zm6 0v-1 1zm11 0h1v-1l-1 1zm17-1h-1l-1 3-1 2v1-1l-1 1-2 10-3 5-3-1-1-1-2-1-3-3-4-3 3-3v-2l1-1v1-1l2-2-1-1-2 2v-2l-1 1 1-2-2 2-5-2-3-2v1l-1-1v1l1 1 1-1v1 1 1h-1-4v1l-1-1v1l-1 1v1l-3 3 1 2h-1v1l-3-1v1-1l-1 1v1-1 1-2l-1-3h-2l-2 1v-1 2h-1-1v-1 1l-1 1 1 1h-2l1 1-2-1v1l-1 1h1l-1 2h1-1-1l-1-1v1h-1l1 3v-1l-1 2-1-4-3 3v2l-4 5-6 1-4 3h-2l-7 3-2 2v-2l-3 8 2 5-1 1v1l-1-1v-2l-1 1 1 2-1 1v-2l2 10-1 4v4l-3 6-1-1-1 3 3 2h4l7-3 9-1 4-2 5-3h3l6-2h6l2 2 1-1 2 1h1v1h1l-1 2h1v5h-1l2 1v-1l5-3 2-2 1-2-1 3-2 2-1 3h-1l-1 1 2-1 3-3-1 3-1 2 3-1v1h-1l1 2-1-2 1 3-2 2 1 3 2 1h2l2 1 6-3v1l-1 1 1-1 1 1h-1l1 2 1-1h1l4-2 7-2 3-5 3-3 4-5 5-3 3-5 5-7v-5l1-4-1-5-1-1-2-1v-4 1l-1-2v2l-1-1v-4l-2-2h1v-1l-6-4v-1-1-2-2l-1-2 1-5-2-2-2 1v-6l-2-5zm-1-1zm0 0zm0 0v-1 1zm0-1zm0 0 1-1h-1v1zm0-1z')
  static Path get au => _$MapSvgData_au;

  @SvgPath('m299 180v-1l-2 2 2-1zm0-1zm1-3h-1 1zm-2 0v-1 1zm0-1zm-3 1 1-1-1 1zm2-1-1 2 1-1v-1zm-1 0zm1-1h1-1zm-4-1h-1 1zm1 1v1l-1-3 1 2zm-2-1-1-1 1 1zm3-1h-1 1zm-4-1zm5 0zm-10 0 1-1h-1v1zm0-1zm4 0zm-3 1v-1l-1 1 1 1v-2 1zm0-1zm7 0-1-1v2l1-1zm-6-2h1-1zm-2-1v2h-1l1 1 1-1-1-2zm5 0-1-1 1 1h1l-1 2 1-2h-1zm-4-1v-1 1zm1-2zm0-2h-3v1l3-1zm0 0 2 1-1 2 1-2-2-1z')
  static Path get bs => _$MapSvgData_bs;
  @SvgPath('m751 179v-1 1zm-4-1zm0 0zm-4 0zm5 0zm3 0zm-4 0v-1 1zm1-1-1 1 1-1zm-1 1v-1 1zm0 0v-1 1zm0-1zm0 0zm0 0zm1 0zm0 0zm0 0v-1 1zm0-1zm-1 0zm1 0zm1 0zm-1 0zm-1 0zm1 1h1l-1-1v1zm2-1-1-1v1h1zm-1-1zm-1 2v-1l-1-1v1 1h1zm-1-2h-1 1zm0-1zm-1 0h1-1zm1 0zm-1-1h1-1zm0-1zm-7-9v2l2 2h-1l-1 2 2 1v2l1 2 1 3 1 1v-2l1 2v-2 1h1v1l1-1v-1l-1-1v-1h1v-1h-1v-1l2 3h1l1 1 3 5-1-1v-1l1 1v-3l-2-5h-1v2l-2-2v-1l2-1 1-2-7-1-1-3-1 1-3-2zm7 14zm1-4z')
  static Path get bd => _$MapSvgData_bd;
  @SvgPath('m257 192h-1 1zm0 0v-1l-1 1h1zm-1-1zm0 0zm1-1h-1 1zm-2-2-2 3-1 6h1l2-4v-2l1-2v-1h-1z')
  static Path get bz => _$MapSvgData_bz;
 
  @SvgPath('m320 317h2l1 3 1-3h3v1l2-9 7-1 3 2v1l1-1h-1l1-5v-2l-2-1v-3h-5l-2-4h1l-1-4-1-1h-3l-3-3h-4l-2-2-1-2v-5h-4l-5 4h-3l3 5-1 1 1 4-1 2v1 2l2 2-2 3v1l1 1 1 3 2 2-1 3 2 3 1 4h2l2-3 2 1h1z')
  static Path get bo => _$MapSvgData_bo;
  @SvgPath('m772 216zm1-1-1 1 1-1zm0 0zm-1-1zm1 0h-1 1zm0-1zm-1 0v-1 1zm0-1v1h1l-1-1zm1 0zm0 0zm0 0v-1h-1l1 1zm-1-1h1l-1-1v1zm1-1zm-3 0zm2 0v-1 1zm-1 0v-1 1zm1 0v-1 1zm0-1zm1 0zm-1 0v-1 1zm1-1zm0 0zm-1 0h-1 1zm1 0v-1 1zm-1-1zm-1 0zm1 1 1-1h-1v1zm-1-1v-1 1zm1-1v-1 1zm-1-3zm0 0zm-13 0v-1 1zm12-1zm-11-2zm10-2zm-8-1zm0-1h-1v1l1-1zm8 0v-1 1zm-12-9v1-1zm0-1h-1l2 1-1-1zm0 0h1l-1-1v1zm-1-1v-1 1zm-1-1v-1h-1l1 1zm8-27h-1v1l-1 2 1 1h-2l-3 2 1 1-1 3v1l-1 3h-2v6h-1v3l-1-1v1l2 3 1-1v2-1l2 1-1 1h2l2 6v5l1-1v1 1-1 1l1-1v1-1l1 1v-1 1l2-2v-1l1 1 1-1v-2l1 3h1l1 5 1 4v-1l2 2v3l1 1-1 1h1v2l-1 1 1 2v-1l2-4-2-4v-2l-3-5 2-4h-1l-5-7h1l1-4h2l1-1h2l2-4v-1l-2 1-1-2h-2v-3h-2l-1-3h1-4l1-1-1-1 2-4v-2l-1-3h-1l-2-3z')
  static Path get mm => _$MapSvgData_mm;
  @SvgPath('m506 227h1v-9h1l2-4-1-4-2-2h-1v1l-1 2h-2l-1 1-1 2 2 1v2l1 10h2z')
  static Path get bj => _$MapSvgData_bj;
  @SvgPath('m963 284zm-19 0h2-2zm0-1zm18 0h1-1zm-11-2zm11 0h-1 1zm-13-1h-1l3 1-2-1zm1 0v-1 1zm15-1zm0-1zm0 0zm-15 0zm-1 0v-1 1zm0-1zm-4 0h-1v1h3l-1-1h-1zm0-1zm1 0zm-3 0zm0 0h-1 1zm3 0-1-1v1h1zm-1-1zm3 0zm-9 0h-1 1zm1 0zm0 0v-1h-1l1 1zm-2-1v1-1zm6 0zm9 0zm-5 1-1-2 2 5-1-3zm-11-2v1-1zm1 0h-1 1zm-1 0zm2 0v-1l-1 1 1 1v-1zm-3 0v-1 1zm2 0-1-1v1h1zm4-2zm0 0h-1 1zm-6 1h1l-1-1v1zm9 1-4-2 4 3v-1zm-4-2zm-2 0zm-6-1zm1 0v-1 1zm1-1zm0 0zm4 1-3-2 1 2h2zm6-5v-1 1zm0-1zm0 0h-1 1zm-1 0z')
  static Path get sb => _$MapSvgData_sb;
  @SvgPath('m368 335v-1 1zm-1-4v-1 1zm1-3v-1l-1 1h1zm1-2zm4-3h-1 1zm3 0v-1 1zm0-1-1 1h1v-1zm2-1v-1 1zm39-9zm-1 0zm-25-22zm0 0v-1 1zm0-1 1-1-1 1zm5-7zm6-13zm0 0zm6-10zm-34-2v-1 1zm0 0 1-1h-1v1zm7-1zm-4-2v1-1zm-20-1h-1 1zm5 0h-1 1zm0 0v-1 1zm12-1zm-21 0h1-1zm-1 0h1-1zm11 0v-1 1zm0 0v-1 1zm10 0v-1 1zm-2-1zm-7 0v-1h-1v1h1zm5-1zm-13 0v1-1zm-1 1h1v-1l-1 1zm1-1 1-1h-1v1zm9-1zm1 0h-1 1zm-10 1 1-1h-1v1zm0-1zm-2 3 1-1v-1l1-1h-1l-1 2v1zm1-3h1-1zm1 0 1-1-1 1zm0 0v-1 1zm1-1h-1 1zm3 0h-3v2h1l-1 1v1 1l4-1 2-2 1-2h-4zm-3 0zm-2 1 1-1v-1l-1 2zm2-2-1 1h1v-1zm0 0zm3 0h-1 1zm-2 0h-1 1zm2-1-2 1h1l1-1zm-2 0h1-1zm0 0v-1 1zm0 0 1-1h-1v1zm1-2h-1v1l1-1zm0 0zm0 0zm-1-3v-1 1zm0-1zm-27-9h-2 1l-2 2-4 1-1 2-1-1h-2l-2-1 1 2 1 3h2v1l-6 5-1-1-2 1-1-2h-1l-1-3-1 2-1-1v1h-4v2h1l1 1h-3v3l1 1 1 2-2 9h-2l-6 3v5l-2 1-1 2 4 5-1 1h2l1 2h2l2-2v5h3 3l5-4h4v5l1 2 2 2h4l3 3h3l1 1 1 4h-1l2 4h5v3l2 1v2l-1 5h1l-1 1 1 3v3l6 1 2 5h2l1 2-1 3h3v5l-5 4-4 6 2-1 3 4 1-1 5 3 3 3-1 1v2l2-2 1-6 2-1-1-3 1 1h1v1l-1 3-2 1v1l3-3 2-6 2-3 1-4-2-3h1l-1-1 1-1h-1l2-1v1-2l4-3h3l2-2v-1h4v-1 1h3l1-2 2-1v-3l3-5v-4l1-2v-2-4-4-2 1-2-1-1-1l1 1 1-1 3-6v1l2-2 3-4 1-5-2-7-5-1-8-7-3 1-6-2-3 1 1-1h-1v1l-1 2v-3l1-1h-1v-1-1h-1l-1 1v-2 1-1h-1l-1-1v1-1h-1l-1-1v1-1h-2v1-1h-1v1l-1-1v1 1h-1v1h1-1l-2 3v-3l-4 1v1h-1l1 1-2-2v1-2l2 1v-1-2h-1v1l-3 1h-2l2-1v1l1-1v-1-1l2-1 3-5v-1l-2-2-1-5-1-2v1h-1l-3 6h-5l-4-1 1 2h-2-2l-5 2-2-1-1-3 1-4v-2h-1v-2z')
  static Path get br => _$MapSvgData_br;
  @SvgPath('m570 111 1-2-4-2-4 2h-3l-3-1-1-1v1l1 2-1 1v2l2 2v1l3-1 3 1h2v-1l3-1h2l-2-2 1-1z')
  static Path get bg => _$MapSvgData_bg;

  @SvgPath('m788 214zm-1-1h-1 1zm10-12v-1l-2 2-1-1-1 1v1l-2-1h-6l-2 2 3 9 1-1 1 2h1 1l2-2 3 1-1-3h1v-1l3-1-1-7zm-11 10zm0 0z')
  static Path get kh => _$MapSvgData_kh;
  @SvgPath('m727 223zm0 0zm-5-5h-1l1 1v-1zm-1-1zm1 0v-1 1zm1-1v1l-1-1 2 1h-2l-1 4 2 6 1 1 3-1 1-3-2-3v-1h-1 1l-2-2-1-2z')
  static Path get lk => _$MapSvgData_lk;
  @SvgPath('m535 261 2 2v-2h2v2h1l3-3 1-2v-4l5-5 1-9 1-4-3-1-2 1-2 4v2l-4-2h-4v3h3l1 1-2 4 2 1-1 4-1 2v-1l-1 1h-2l-1-2v1h-3l1 5-1-1-1 2 3 3 2-2z')
  static Path get cg => _$MapSvgData_cg;
  @SvgPath('m571 231-1-1-2 2-1-1-3 2-1-1-1 2-5-1-4-2-2 2v3l-1 4-1 9-5 5v4l-1 2-3 3h-1v-2h-2v2l-1-1-2 1v2h-1l1 1h2 2 7l1 4 2 3 5-1 1-3h3l-1 1h4v7l1 3v3l5-1 1 1h3v1l1 1h3l1-1 1 2 2 1 2 3 1-1 1 1v-4l-1 1-3-3 1-3-1-5 2-2 5-1-3-6-1-6v-2l-1-3v-1l2-4 1-7 4-4-2-1v-3l-3-4-4 1-2-2h-5zm-36 35z')
  static Path get cd => _$MapSvgData_cd;

  @SvgPath('m803 183-4 1-1 2 1 2 2 1h1l2-4-1-2zm0 3zm17-5h-1 1zm-17 0h-1 1zm-5-1zm4 0v1-1zm0 0h1-1zm4-1v-1 1zm2 0v-1 1zm0-1zm2-1h-1 1zm-1 0h1-1zm1-2zm0 0h-1 1zm9-2zm1-1h-1 1zm1-2v-1 1zm3-3v-1 1zm1-1zm-2-1h1l-1-1v1zm4-7h-1 1zm1-3h-1 1zm-1-2h1-1zm1-1v1-1zm-1 0h-1 1zm1 0h-1 1zm-1-1zm1 0h-1 1zm-3-3zm-1-1-1-1 2 1h-1zm-5-2h-1 1zm-6-23zm-22-43h-5l-1 2h2l1 2v5l1 1-3 2-3-1 1 6h5v-1h2l4 3 2 2h-6l-2 1-1 2h-2l-1 2-5-1v3l2 2-2 3-3 1-4-1-5 3-13-3h-11l-5-5-5-2-7-1-1-1-1-3-4-4-4-1-2-1-1-2h-1-2l1 2h-2l1 4-2 1-4-1v5h1v1h-2l-4 1 2 2 2 3-1 1h1l-1 1 1 1-5 4h-3v2h-2l-1-1h-1l-2 2v2l1 3h3v3h1v1h-1v1h1l3 1 1 3 5 1h1l1 2 2 1v3l3 2-1 2-1-1h-1l2 4 1-1 6 4v1l1-1h1l5 4 2-1 1 2h2v1h2l1 2v-1l1 1v-1l2 1h2l2-1 1 2 1-2 1-1 2 1h2l1 1h2l5-5 2 1 2-1h1l-1 1h1l1 1-1 1 3 1v-1h1l2 3h1l1 3v2l-2 4 1 1-1 1h4-1l1 3h2v3h2l1 2 2-1v1l1 1h1l-1-3 1-1h1v-1l2 1 1-1 2 1 3-3 1 1 3 1v2l4 2h2l-1-1 2 1 1-1 2 1-1 2 1 2h1l1-1-2-1 1-2h3l1-1h3v-1h1l-1-1 1 1v-1l-1-1v-1h1-1l2 2h1l6-2v-1h-1l4-3h-1 1v-1h1v-1h1v-1h1-1l1-1 1 1-1-1v-1h-1v-1l1 1 1-1h-1l-1-2 2 1-1-1 1-1 1-2-1-1 2-1-1-1h1l-1-1h2l-1-2-1 1 1-2h-1l-1-1h-3l4-2-5-3h-1l-2-1h1l1 1 1-1 4 1-4-2-4-6-3-1 1-4-1-1 2 1v-2l2-1h2l-1-2h-2l-3-1-2 2h-1-1l-2-3-3-1-1-2h3l3-6 4 2-2 3h2v2l6-4 3-3v-2l4 1-1-2h2l1-1v-2l3 2-1-1 1-1v-1l-4-5 1-1h4l-2-6 1-2-2-1h-3l-1 1h-4l-3-2v-1l-10-3v-1l-10-8-8-2zm5 99z')
  static Path get cn => _$MapSvgData_cn;
  @SvgPath('m695 129-5-1-3 3-1-2v-2h-1v-1l-2-1-1 2v1h-2v1h-1l-2 1-1-1h-3l-2-1-3 1v3l-3 1v2h-1l-1 1-3-2v4l-1 1 2 2-1 1 2 5 2 1 1 1-2 3 4 2 10-2v-3l1-1h2v-1l1-1 2 1 1-1v-4l2-1-2-2h3v-1l1-3-2-2 3-3h5v-1h1z')
  static Path get af => _$MapSvgData_af;

  @SvgPath('m342 423zm1 0zm0 0v-1 1zm0-1zm0 0h-1 1zm-1 0h-1 1zm-5-1h-1 1zm-1 0zm7 0zm1 0zm-1-1h-1 1zm-4 0h-1l2 1h-1 1l1 1-3-1h-1l1 1-2-1 3-1zm1 0h2l-2 1-1-1h1zm-6 0h1-2 2-1zm2 0h-1 2-1zm-3 0h-1 1zm1 0h-1 1zm-4-1h1-1zm-1-1zm5 0h-1 1zm-3-1h1v1h-2l1-1zm-2 1h-1l1-1v1zm-2-1h-1 1zm3 0h-1l1 1-1-1h1zm3 0h-1l1 1-1-1v-1l1 1zm-9-1v-1l1 1h-1zm2-1 1 1v-1l2 1-2 1-1-1h-1l1-1zm-1 0v1l-1-1h1zm-2 0zm1-1h1-1zm7 0zm-9 0-1-1 5 2-4-1zm11 5-3-1 4 1-2-2 2 1 1 1v-1h1v1h1v-1l-4-1 2-2h-3v-1l-1-1 2 1v-2l3 1 2 6v1l-5-1zm-10-6h-1l1-1v1zm-2-1h-1 1zm2 0h-1 1zm-1-1v1l-1-1h1zm-2 1v-1h1l-1 1zm1-1zm-1 0h1-1zm3 1-1-1 1 1zm-3-1zm-1 0v-1 1zm2 0-1-1 1 1zm1-1zm-2 0v1l-1-1h1zm1 0h1-1zm-2-1-1 1v-1h1zm2 0h-1 1zm1 0h-1 1zm-1 0zm-1 0h-1 1zm-2 0v-1h1l-1 1zm2 0-1-1 1 1zm0 0v-1 1zm-1-1h1-1-1 1zm0 0-1-1 1 1zm-1-1h-1 1zm0 0zm-1-1v1h-1l1-1zm2 0 1 2-2-2h1zm-1 0zm-1-1-1 1h1l-1-1h1zm-1-1v1l-1-1h1zm-1 0v-1 1zm3 0-1-1 1 1zm-2-1-1-1h1v1zm-1-1h-1 1zm0-1v1h-1l1-1zm0 1v-1 1zm0-1h1-1zm-1 0zm1 0zm-1 0-1-1 1 1zm2-1h1-1zm1 0zm0 0h-1 1zm-1 0-1-1 1 1zm0 0 2 4-1 1v-1-1h-1l1 2-2-2h1v-1l-1-1h1l-1-1h1l-1-1 1 1zm-2 0 1 1-2-2 1 1zm1-1v1l-1-1h1zm0-1v1h-1l1-1zm2 0h-2 2zm-3 0h-1 1zm0 0zm1-2zm0 0h1-1zm0-3zm0 0v-1 1zm-2-1-1 1v-1h1zm2 0zm-1 0zm-2 0v-1 1zm1 0-1-1h1v1zm1 0v-1 1zm-1 0v-1h1l-1 1zm2 0v-1 1zm-1-1zm-1 0zm1 0zm-1 0-1-1h1v1zm1-1h-1 1zm-1 0zm1-1v1h-1l1-1zm-1 0h-1 1zm0 0zm1 0h-1 1zm-3 0h-1 1zm3 0zm-2 0zm1 0-1-1h1v1zm-1 0-1-1 1 1zm-1-1zm3 1v-1 1zm-2-1h1-1zm0 0h-1 1zm1 0h-1 1zm0 0zm3 0h-1 1l-1 1-1-1h1 1zm-2 0h-1l1-1v1zm-1 0-1-1h1v1zm2 0-1-1 1 1zm-2-1zm0 0zm-1 0h-1 1zm0 0h-1 1zm1 0h-1 1zm0 0h-1 1zm-1-1h-1 1zm1 1v-1 1zm1-1zm-2 0h1-1zm-2-1zm2-3h-1 1zm0 0-1-1h1v1zm1-1h-1 1zm1 0v-1 1zm-4-1h1l1 1h-1l1 1h-1l2 2-1 1-2-1v-4zm2 0v-1 1zm-27-26zm5 0h-1l1-1v1zm-87-21h-1 1zm10-2zm69 0zm0-1h-1 1zm25-27 1 1 1 3 2 2-1 3 2 3 1 4h2l1 1-1 3-3 3 1 1v4l1 1-1 1-2 4-1 3 1 3-1 3 3 6 1 1v3l-1 3 1 3-1 2 2 5-1 2v2 3l2 5h-1l1 2 1 1 1 4h2v1h-2l2 1v1l1 4v1l-1 1v1l1 2v1l-1 2 2 4h2l1 3 2 1h4l5 1h-3l-3 1 1 3v1l-4-2h2v1l1-3-3 1 1 1h-1l1 1-3-1h2-1l1-1h-1-1l1-1h3-4 1l-1 1h1-1l-1-1h1-1l-1-2 1 1v-1l1 1v-1l1 1h-1 1l-1-2-2-1 2 1h-2l1 1-1-1 1 1h-1v-1h-1l-1-1 1 1-1-2h-1l1-1v-1l-1 1-1-1 1 1-2-1h2l-2-1h2l-2-1h2l-2-1v-1 1l-1-2h1-1v-1l-2-1h3 1l-1-1 1 1-2-2v1h-2 1l-1-1 1-1-1-1h-2v-1l-2 1h1-1l2-3h1l1 1h-1-1 1 1v1-1 1h1v-1 1l-1-2h1-1v-1h2l-2-1v-1h1v-1l-2-1 1-1-1-1v-2-1l-1-1 2 1-1-2h-1l1-1h-1-1v1l-2-1h1-1l-1-2 1-5-4-7h2l1-15-2-5v-3l-1-3 1-4v-8-5h-1l1-7-2-10h1l1-2zm7 90zm10 20z')
  static Path get cl => _$MapSvgData_cl;

  @SvgPath('m526 236zm14-29-1-1h-1v2h2v2l-3 2-2 7-2 1-1 5-1 2-2-2-3 1-2 3-1 3v1l1-1v2l2 1v5h1 4 3 2 4l4 2v-2-2l-4-6-1-4 3-5-1-3-3-3 1-2h4l-2-2v-4l-1-2z')
  static Path get cm => _$MapSvgData_cm;
  @SvgPath('m539 206 1 1 1 2v4l2 2h-4l-1 2 3 3 1 3 9-1 1-2v-1l4-1 4-4-1-1 4-1-2-5h-1l1-2-1-2 2-1-1-1 2-2v-1l2-1v-12l-22-12-3 1 1 5 2 4-1 10-6 8 1 3 1 2h1z')
  static Path get td => _$MapSvgData_td;

  @SvgPath('m280 239zm1 0zm0 0zm0 0v-1 1zm1-2zm0-3zm0 0h1v-1l-1 1zm18-26-5 3h-2l-1 2h-1l1-1h-1l-2 1-1 4-4 3 1 2-2-2 1 2-2 1v1l1 2v1 2l-1 1h1v3 1h-1l2 1-3 4h-1-1v2l-1 1v1h1l3 3h4l4 2 1 2 2 1 2 4 3-1 4 1 1 1-2 3 2 1 2-9-1-2-1-1v-3h3l-1-1h-1v-2h4v-1l1 1 1-2 1 3h1l-1-4-2-1 2-2-2-3 2-6-5 1-3-3h-5l-1-2v-2l-1-3h-2l3-6 2-2h2v-1l-1-1zm-21 31zm-7-32zm1-2z')
  static Path get co => _$MapSvgData_co;
  @SvgPath('m262 215zm0-3h-2v1h1l-1 1v2l2 1v-1-1h1v2l3 1-1 2 2 1v-2l1 3v-3-2h1l-3-5-1 1-3-1z')
  static Path get cr => _$MapSvgData_cr;
  @SvgPath('m552 220-1 2-9 1-3 5 1 4 4 6v2l2-4 2-1 3 1v-3l2-2 4 2 5 1 1-2 1 1 3-2 1 1 2-2 1 1h5l-1-2-2-1v-2l-4-3v-1l-2-1v-1l-2-1v-3l-2-4-4 1 1 1-4 4-4 1v1z')
  static Path get cf => _$MapSvgData_cf;
  @SvgPath('m284 182zm0 0zm0-1h-1 1zm-1 0zm9 0h-1 1zm-9 0h-1 1zm-1 0zm0 0v-1 1zm-1-1zm0 0zm-6-1 1-1-1 1zm-2 0-1-1h-1l1 1h-1 1 1zm13-2v1-1zm0 0-1-1v1h1zm0 0v-1 1zm-1-1v1-1zm0 0h-1 1zm-2-1zm-1 0h-1 1zm-2-1h1-1zm0 0h-1 1zm0 0zm0 0zm-2 0zm-3 0-6 1-1 2-2 1h3l4-3h2v1h-1l1 1h4l1 1h3l2 3 2 1v1l-2 1h7 1l2-1-4-2v-1h-3l-1-2v1l-7-5h-1-2-1-1zm4 0z')
  static Path get cu => _$MapSvgData_cu;
  @SvgPath('m431 200v-1 1zm3 0-1-1v1h1zm2-4h-1 1zm-3-2h-2l1 1 1-1zm-3 0v-1 1zm-1-1h1-1z')
  static Path get cv => _$MapSvgData_cv;
  @SvgPath('m66 317v-1 1zm-5-3h-1 1zm6-3zm-3 0v-1 1zm0-1zm1 0zm-4-2zm-2-1zm-10-3zm-10-20zm-2-2zm13-2zm1 0h-1 1zm-1-1zm9-3-1-1v1h1z')
  static Path get ck => _$MapSvgData_ck;

  @SvgPath('m526 75h-1 2-1zm1-1h-1l1 1v-1zm-4 1v-1 1zm5-1h-1 1zm-4 0zm-2 0h1-1zm2 1 1-1h-1v1zm10-1-1-1v1h1zm-15-1h-1 1zm5 0v-1l-1 1v-1l-1 1v-1 1 1h1 1v-1zm4-1zm-4-1h-1v1l1-1zm4 0h-1v1l-1-1-2 1 1 1 2 1 1-1h-1l1-2zm-2-2zm-6-1-1 1 1-1zm2 0h-2l1 2-1-1-1 1-1-1v2 1l1 1v1l2 1 1-1h-1l1-1h-1l1-1h-1 1l2-2-2-1h1v-1h-1zm3-1h-1 1zm-2-1-4 2-1 1h1-1l1-1h1 2 1v-2z')
  static Path get dk => _$MapSvgData_dk;

  @SvgPath('m302 191zm8-1-1-1v1h1zm-6-6-2 1-1 3v2l1 1 1-2h1 1 1 4l1-1-1-1h-3l1-1h-1v-1l-3-1z')
  static Path get _do => _$MapSvgData__do;
  @SvgPath('m275 257v-1 1zm1-2h-1 1zm-28-3v-1 1zm-2-1zm1-1zm2 0v-1l-1 1h1zm-3-1h1-1zm-3 0v-1h-1l1 1zm3-1h-1 1zm-2-1h-1l1 2-1 1 2-1-1-2zm2-1zm-1-1zm34-2h-1l-3 1v3l-1 1v1l-1 1v4l1 2 2-3v2l-1 2-1 1 1 1-1 1v1h2l2 2 1-1 1-4 5-3 3-3v-2l-1-3h1l-2-1h-4l-3-3zm-1 0z')
  static Path get ec => _$MapSvgData_ec;
  @SvgPath('m591 160zm-6-12zm4 0h-1 1zm-1 0h-1 1zm-21-1h-1l-1 4 1 3 2 23h17 15l-3-3-1-3h1l-2-2-5-10-4-6 1-1 2 5 3 2 1-5-2-6h-2-1v1-1l-1 1-2-1v1l-1-1v-1l1 1-1-1h-2l-5 2-10-2zm24 15z')
  static Path get eg => _$MapSvgData_eg;
  @SvgPath('m476 79zm0-2h-1 1zm3-3zm3 0 1-1h-1l-1 1v-1l-1 1-2 1h2l-1 1h-4v1-1l1 1v1l-1 1h3l-3 2h3l-4 2h2l-2 1h2-1 1l-1 1h2l5-3 2 1 1-3-1-3-2-1-1 1-1-1 2-2zm-3 0z')
  static Path get ie => _$MapSvgData_ie;

  @SvgPath('m554 65h-1 1zm-3-2-2 1 1 1h-1l3-1-1-1zm1 1v-1h-1l1 1zm0-2h-1 1zm-2 0h-1l2 1-1-1zm7-2-5 2v1h1-1l1 1h2l-1 2 3-1 3 2h2v-1l-1-3 1-2-5-1z')
  static Path get ee => _$MapSvgData_ee;
  @SvgPath('m610 197v1h1l-1-1zm0-1zm-4-5v-1l-4 3h-1v3l-1 3v3l2-1 1 1 1-2 2 1h1 4l6 7 2-1-6-6-3-1-1-2v1l-3-8z')
  static Path get er => _$MapSvgData_er;

  @SvgPath('m600 202-1 5h-1l-2 6h-1v1l-1 6h-2l-1 2 2 1 3 3 2 4h1v2l4 1 3 3h4l3-3 1 2 2-1 5-2h4l8-10h-3l-8-3-4-4v-2l1-1h-4v-2l2-2-6-7h-4-1l-2-1-1 2-1-1-2 1z')
  static Path get et => _$MapSvgData_et;
  @SvgPath('m533 93-3 2 1 2-1-1-4 1h-1l-1 1-1-1v1l2 1h4 1l3 1h2l4-1 1-1v-1-1h1v-1-2l-5-1-1 2-2-1z')
  static Path get at => _$MapSvgData_at;
  @SvgPath('m535 94 1-2 5 1 4-2-1-2h-2v-1h-2v1h-1l-1-1 1-1h-4l-1-1-6 2 2 3 3 2 2 1z')
  static Path get cz => _$MapSvgData_cz;

  @SvgPath('m550 59v1l1-1h-1zm-1 0zm-2 0zm1 0h-1 1zm-1 0h-1 1zm1 0zm9 0zm-7 0h-1 1zm-1 0zm0 0-1-1v1h1zm0-1zm-1 0zm-1 0zm0 0h-1 1zm-1 0zm0 0zm0-1zm-1-7zm2 0zm-2 0zm3-2zm4-3h-1 1zm-1-2zm5-11h-4-1v2l-1 1h-2-3l-3-2-1 1 3 1 4 2v1 1l1 1v1l1 2h1l2 1v1l-2 1-2 2-2 1v1h-1v1l-1 1 2 3-1 2 3 1v1h1v-1 1h1l-1 1 7-1 1-2v1h3l7-7-4-2 1-2-3-2v-2h1l-3-3 1-2-3-2-1-1h1l-1-1h1-1l1-2h-2zm0 27zm-4 1h-1l1-1v1z')
  static Path get fi => _$MapSvgData_fi;
  @SvgPath('m8 313v-1 1zm1-3zm-1-2zm986 0zm-987 0zm984-1h-1l-1 1 2-1zm-985 0zm-2 0zm987 0zm-984-1zm988 0zm-6 0zm1-1zm-984 0zm988-1h-1 1zm-989 0zm2 0zm987 0v-1 1zm-990-1zm989 0h-1 1zm-989-1zm987 0-2 1-1 1h4l-1-2zm-3 0zm-984 0zm990 0h1-1zm-989 0-1-1 1 1zm984-1h-1 1zm3-1zm4 1 1-1-1 1zm-994 0v-1 1zm987-1h1-1zm7 0h1-1zm1-1zm-996 0zm996-1-5 2 1 1 2-1h1l1-1-2 1 2-2zm-996 0zm990-11z')
  static Path get fj => _$MapSvgData_fj;
 
  @SvgPath('m356 412zm3-1zm-7 0v-1 1zm1 0v-1l-1 1h1zm-1-1zm5-1h-2-1-1l2 1h-1 1l-2 1 2 1 2-3zm1 0-2 2 1 1v-1h1-1 2l-1-1h2-1 1v-1h-1v1l-1-1zm-3 0zm-1 0zm2 0h-1 1z')
  static Path get fk => _$MapSvgData_fk;
  @SvgPath('m929 230h-1 1zm26 0h-1 1zm-56-4zm17 0zm24 0zm0 0zm1 0v-1 1zm0-1zm-18-1zm-7 0zm0 0h-1 1zm-15 0zm21 0zm1-1v1-1zm-16 0zm10-3h-1 1zm1 0zm0-1zm-34-2z')
  static Path get fm => _$MapSvgData_fm;
  @SvgPath('m111 336v-1 1zm-2-1-1-1 1 1zm-13-12zm-5-2h-1 1zm39-1zm0 0zm-49-1zm4-1zm34 0v-1 1zm-5-2h-1 1zm-14-6zm-17-7zm4 0h-1l1 1v-1zm-2 0v-1 1zm1-2zm0 0zm-4-1zm-1 1-1-1v1h1zm1-1h-1 1zm-2 0zm-1-1zm15-1zm-5-1zm10 0zm14-16v-1 1zm-2-2 1-1h-1v1zm1-1h-1 1zm-3-1h-1 1zm1-2zm-2 0zm-2-2 1-1h-1v1zm1-1z')
  static Path get pf => _$MapSvgData_pf;
  @SvgPath('m523 112zm0 0v-1 1l-2 1 1 2 1 1v-2-2zm-27-10v-1 1zm0-1h-1 1zm-3-2zm1-1h-1l1 1v-1zm-3 0v-1 1zm-4-4h-1 1zm3-1zm15-7-2 1v2l-4 2h1-4v-1h-2l2 3-4 1-1-1-4 1h2l-2 1 1 1 3 1h1 1v1h1 1-1v1l2 2v1 1h1l1 2-2-2v3l-2 4 4 1 5 1h1-1l4 1v-2l2-2h3l3 2 3-2v-1 1-1-1h-1v-2l-1-1h1l-1-2h1l-1-2-2 1 2-4h2v-2l1-3-4-1h-2l-2-1v-1h-2v-1l-4-2z')
  static Path get fr => _$MapSvgData_fr;

  @SvgPath('m524 250v-1 1zm10-10h-3v4h-5v2l-1-1 2 1h-2v2l-1 2-1-1 3 3-2-1 2 4h1-1l4 5 1-2 1 1-1-5h3v-1l1 2h2l1-1v1l1-2 1-4-2-1 2-4-1-1h-3v-3h-2zm-8 6z')
  static Path get ga => _$MapSvgData_ga;
  @SvgPath('m605 113 1 1-1 1h4l1 2 4-1h1l3 1 1-1-2-1 1-1-5-2h-2l-3-2-7-1-1 1 4 2 1 1z')
  static Path get ge => _$MapSvgData_ge;
  @SvgPath('m501 229-1-1h1v1l1-1-2-3 1-4-1-2 1-2h-1v-2l-1-3h-2-6l1 5v4l-2 5 1 3v2h-1l3 1 2-1h2l3-2h1z')
  static Path get gh => _$MapSvgData_gh;

  @SvgPath('m401 60v-1l-1 1h1zm1 0v-1 1zm-2 0 1-1h-1v1zm2 0v-1 1zm0-1h-2 3-1zm-2 0zm-2 0zm0 0v-1 1zm-1-1v-1 1zm0-1h-1 1zm-1 0zm-3 0h-1 1zm2 0zm-1 0zm0 0zm1 0h-1 1zm0 0h2-2zm0 0zm0 0h-1 1zm1 0v-1 1zm-3-1h-1 1zm-2 0zm1 0v-1h-1l1 1zm13-1h1-1zm-14 0v-1 1zm-1-1h1-1zm16 0h1-1zm-16 0h1-1zm0-1zm17-1zm1-1h-1 1zm1 0zm-20 0zm20 0v-1 1zm1-1h-1 1zm-21 0zm0 0h1-1zm21 0-1-1v1h1zm-22-1zm-1-1 1-1h-1v1zm25-1h-1 1zm-1 0h-1 1zm-23 0h1-1zm1 0 1-1-1 1zm0 0 1-1-1 1zm-1-1zm25 1v-1l-1-1 1 2zm0-2zm-25 0zm27-1zm1 0zm-29 0zm30 0zm4 0-1-1v1h1zm-34 0v-1 1zm34-1h-1 1zm-1 0h-1 1zm1 0h1-1zm1 0h1-1zm0 0v-1 1zm-36-1h1-1zm38 0h1-1zm4-2zm-40 0h-1 2-1zm42-1h-1 1zm0 0zm-42-1v-1 1zm1-1h1-1zm5 0h-2 2zm44-1h-1 1zm-46 1 1-1h-1v1zm-2-1h1-1zm1-1h1-1zm0 1v-1 1zm2-1h-1 1zm2 0h-1 1zm-2 0h-1-2 3zm59-3h-1 1zm-55 0zm-1 0h-1l1 1 1-1h-1zm-3 0-3-1h-1l1 1h-2 1-1l3 1h-2l1 1 4-2h-1zm-3-1-1-1v1h1zm6-1zm48 0 1-1h-1v1zm4-1h-3l-2 1h4 2l-1-1zm-52 0h-1 1zm53 0h-1 1zm0 0v-1h-1l1 1zm-57 0h1v-1l-1 1zm3-1h-1 1zm-1-1h1-1zm-4 0h-1 1zm0-1h-1 1zm1 0 1-1-2 1h1zm1-1h-1 1zm-1 0h-1 1zm1 0h-1 1zm0-1zm-1 0h-1 1zm0 1 1-1-2 1h1zm-1-1h1-1zm63 0h-2l4 2h1l-1-1h1l-3-1zm-62 0h1-1zm60 0h-1 1zm4 0h-4 5-1zm-64 0zm1 0h-1 1zm66-1h-1 1zm-68 0zm0 1 1-1-1 1zm2-1h-1 1zm-1 0zm1 0zm60 0h-2-1 6-4 3-2zm-60 0-1-1 1 1zm-2-1h1-1zm1 0h-1 1zm0 0zm1 0h-2 1 1zm69-1h-1 1zm-2-1-2 1h4l-2-1zm-67 0h-2 2zm-1 0zm0 0h-1 1zm72 0zm1-1v1-1zm-74 0zm71 0h-1 1 1-1zm5-1h-2l-1 1 3-1h-1 1zm-88-2h1-1zm2 0zm4 0zm77 0 1-1h-1v1zm-91-1zm91 0h1-1zm-1 0h-1 1zm1 0h-1 1zm-90 0h-1 1zm90 0h-1 1zm-1 0h-1 1zm3 0v-1 1zm2 0-1-1v1 2l1-2zm-2-1zm-1 0zm-94-1h1-2 1zm3 0h-1 2-1zm8 0h-1 1zm84 0v-1 1zm-85-1zm88 0h1-1zm-3 0h-1 1 1-1zm1 0zm-3 0v-1l-1 1h1zm1-1zm2 0zm1 0zm-4 0h1-1zm1 0h1-1zm2 0zm1 0h-1 1zm-1 0zm1 0h-1 1zm0 0v-1 1zm0-1zm1 0zm1 0zm-2 0v-1l-1 1h1zm2-1h1-1zm-2 0zm-1-2h2l-1-1-1 1zm-80-1zm80-2h1l-1-1v1zm4 0-1-1 1 1zm-2-1h-1-1 2zm1 0h-1 1zm-55 0h1l-3-1 2 1zm0 0v-1h-1l1 1zm6-1h1-1zm4 0h-2l3 1h1l-1-1h2-3zm10 0zm-16-1zm4 0h-1 1zm2 0h-1 1zm12 0h-2 2zm2 0h-1 1zm-3 0h-1-1 2zm0 0h-1 1zm-2 0h-1 1zm-1 0h-1 1zm4 0h-2 3-1zm-3 0v-1h-1l1 1zm4-1h-1 1zm11 0h-11l3 1h-4 1-6-5 3-6l11 1h1-3-1v-1h-6l5 1h-3l-1 1h-1 1l-9-1h-1l2 1h-3l2 1-5-1-2 1 1-1-1-1-9 1 4 1-3-1h-3-2l1 1h-1v1h-4-1l-8 1 1 1h5-2l-1 1-2 1-13 1-1 1h-1l2 1h3-2 3-2l1 1 2-1v1l3-1 1 1h-1-5 5v1h-9l5 1h-3l1 1h3-1 3 1-1l5-1 8 2v1h-1l3 2h-1 1-1 1l-2 1 2-1-1 1 1 1h-1l2 1-2 1h2l-1 1h1-1 1-3 2l-2 1h3l-4 1 1 1h2v-1h2v-2 2l2-1-2 1h3l-3 1h4-3 2l-2 1h2 1-2l2 1h-2 2-2 1 1l-1 1-6-2-1 1 4 1h4l-1 1h1l-2 1h1l-2 1 2-1v1h-1 1-2l-1 1h2-1-3l-2 1h1l4-1-1 1h-1l3 1h-1-1-2l-1-1v1l-2-1 3 1h-2l-2 1 6-1h-1 2l-2 1h1 1 1-1-1 1-1-1-1l-1-1-4 2h2l4-1-1 1h2-1-1-1-3-1l3 1-3 1h2-2l-1 1h2-1l5-2 2-1v1h1-2 1-2l-5 2h3l-3 1h1l-1 1 1-1v1h1v-1h1 1 1-1-1-1 1l-2 1v2l2-1-2 1-1 1h1-1l3-1h1v-2 2l2-1-1 1 1 1-3-1 2 1h-1-1-2 4l-4 1h2l-2 1h2-2l2 1h-2 3-2l1 1h-1 1 1-1-1l1 1v-1l-1 1 2 1-1 1 2-1-2 1h2-1l1 1h-2 3-2 1v1h1-1 1-1 1l-1 1h2-1 2 1v-1l1 1v-1h1-1l1-1v1l-1 1 2-1-3 1h3-2 1l-1 1 1-1v1l1-1-1 1 2-1-2 2 2-1-2 1h1 1 1v-1l2 1v-1h-2l3-1h-2 2-1l2-1h-2 2l-1-1h1-1 2l-1-1h2-1l1-1-2-1h2l-2-1h3 1l-1-1h1l-1-1h1l1 1-1-1 1 1v-1 1-1h1l-2-1 3 1-1-1h1l-2-1h1 1 1l-2-2 4-1h-1l4-1h1l-1-1 1 1 1-1-1-1h2l-2 1 2 1v-1 1l1-1h1l-1 1 2-1v-1h2 1v-1 1l1-1 2-1h-1l1-1 3-1-1-1h1v-1 1h1v1h3v-1h1 1l6-1 5-2h1v-1l4-1-6-1-5 2-2-1h4l-5-1 2-1h3l3-1-6-1h2l-2-1 7 2 2 2h2v-1 1h2l-1-1h1-1l1-1h-2l1-1h1-3 2l-4-2-2 1 1-1h-2 3v-1l-4 1 1-1h-2 5l-3-1-2 1-1-1h3l-2-1 3 1h1l2-1-2-1 2 1h1 3-3l3 1h4v-2h-3v1-1h-1l1-1h-1 2-1l5 1 1-1h-4l1-1-3-1h3l-3-1 2 1-2-1 5 1h1l-1-2h-4 4-3v-1h-1 1-2 2l2-1h-2 2 4 1v-1h-5l4-1h-1-2l-1-1-1 1h-1l3-2h-1 1-1l3-1h1v-1l-1 1 1-1 4-1h-3l-2 1-1-1 2-1h4 2-9l10-1 6-1-7-1h-2 1l-2 1h-3l-8 2 4-2 1-1h-3-1l-5 1h-1l5-1h-14 6-3 11l7-1-5-1h-3 2-18l17-1h-11zm17 12h-1 1z')
  static Path get gl => _$MapSvgData_gl;
  @SvgPath('m521 96h-1 1zm-6-18zm0 0h1-1zm1 0zm2 0zm-1 0zm0 0zm2-1zm13 0zm-13 0zm13 0h-1 1v1l1-1h-1zm-7-1zm-5 0zm6 0h-1 1zm-7 0v-1 1zm11 0v-1 1zm0-1 1 1-1-1v1h1l-1-1zm-11 0h-1 1zm0 0zm6 1h-3v-1h-1l-2-1-1 1 1-1 1 2h-1 1v1l2 1-3-1v2l-1-1h-2v1 2h-1l1 1-1 1-2 1 1 1-1 1 2 2-1 1 1 1v1l4 1-1 3v2h2v-1l3 1 1 1 1-1h1l4-1 1 1-1-2 3-2-3-2-2-3 6-2 1 1v-2l-1-1v-3h-1v-3h-1l-2-2h-2 1-1l-3 1v-1zm1 1zm0 0zm3-1z')
  static Path get de => _$MapSvgData_de;

  @SvgPath('m563 137zm7-2zm-8-1h-1v1l3 1h4l-6-2zm9 1v-2l-1 1 1 1zm-11-1v-1 1zm0-2h-1 1zm7 0zm-1 0zm6 1 1-2-1 1v1zm-1-1v-1 1zm-5 0v-1 1zm2-1zm4 0zm-8 0zm1 0zm-9 0zm7 0v-1 1zm3 0-1-1 1 1zm-2 0v-1h-1l1 1zm-8 0v-1 1zm7-1zm3 0zm1 0h-1 1zm3 1 1-1h-1v1zm-4-1zm-1 0h-1 1zm-1 0h-1 1zm6 0-1-1 1 1zm-3-1h-1 1zm-2 1v-1 1zm4-1zm-3 1v-1h-1l1 1zm-3-1zm6 0zm-9 0h-1 1zm0 0h1-1zm3 0v-1 1zm2-1h1-1zm-1 0v1-1zm-4 0zm5 0h-1 1zm-3 0h1-1zm5 0 1-1-1 1zm-7-1zm10 0h-1-1 2zm-17 0v1-1zm11-1-1 1h1v-1zm-4 1zm-7-2-1 1h1v-1zm0 0zm14 1v-1h-1l1 1zm-14-1zm13 0h-1 1zm-13 0v-1h-1l1 1zm10-1-1-1 1 1zm-3 0h-2l5 3-1-3h-2zm1-1h-1 1zm-1 0zm-9 0zm10 0v-1 1zm6-1-1 1h2l-1-1zm-3 0v-1 1zm-14-1 1 1-1-1zm-1 0zm16-1-1 1 1-1zm0-1h-1 1zm-2-1h-1 1zm3-3v1h-2l-3-1-3 1-5 1-1 3-1 1 2 2h1l-1 1h1v2l3-1 2 1v1l-3-2-2 2 2 1v2h1l1 2v-2l2 2-2-4h2l-1-1 1-1 2 2v-2l-5-2 2-1-1-1 1 1-2-3v-1l1-1 2 2-1-1 2 1-1-1 2 1-2-2 4-1 2 1 1-2-1-1z')
  static Path get gr => _$MapSvgData_gr;

  @SvgPath('m461 216 1 2 2-2 4-1 2 3-1 3 1-1 3 1v3h1l1-1 2-1-1-2 2 1-1-2v-1l-1-1 1-2-1-3h-1l1-1h-1l-1-3-2 1-2-1-1 1v-1l-1 1-1-1h-2l-4-1-1 2 1 1-3 1-1 1h1v2l3 2z')
  static Path get gn => _$MapSvgData_gn;
  @SvgPath('m336 225zm-4-4v-1 1l-2 2 1 2-3 1-1 2 2 3h2v2h1v2l-1 4 1 3 2 1 5-2h2l-2-5h-1l-1-2v-2l2-1v-1-2l-3-3-1 2 1-3-4-3z')
  static Path get gy => _$MapSvgData_gy;
  @SvgPath('m296 190zm2-2v-1h-1l1 1zm1-4-2 1h2v2l1 1h-6v1l1 1 2-1 4 1v-2l1-3-3-1zm-3 4zm3-4v-1l-1 1h1z')
  static Path get ht => _$MapSvgData_ht;
  @SvgPath('m256 205zm0 0zm5-9-6 1-3 2-1 2 2 2h2v2h1v1l2-1v-2l2-1 1 1 3-3 1 1 3-1h1l-3-2h1l-1-1-5-1zm-2 0h-1 1zm0-1h1-1zm2 0h1-1zm6-3zm0 0z')
  static Path get hn => _$MapSvgData_hn;
  @SvgPath('m542 112zm2 0-1-1 1 1zm-2-1h-1 2-1zm1 0h-1l4 2v-1l-2-1h-1zm-3 0zm1-1h2-2zm1 0h-1 1zm-1 0h-1 1zm-2-1h-1 1zm-1-1h-1 1zm0 0zm-1 0v-1 1zm-1-1 1 1-1-1zm0-1 1 1-1-1zm0 0h-1 1zm0-1v1-1zm-1 0v-1 1 1-1zm1 0-1-1 1 1zm4-5-2 1v1l-1 1h-2-2l1 2 1-1 4 5 3 1 2 1-4-4-1-3h6l2 1 1-1h-1l-1-2h-3l-3-2z')
  static Path get hr => _$MapSvgData_hr;
  @SvgPath('m546 102 3-1h2 1l1-4 2-1-2-2h-3l-5 2-4-1v1h-1v1 1l-1 1 1 1 3 2h3z')
  static Path get hu => _$MapSvgData_hu;
  @SvgPath('m467 42 1-1-1 1-3-1v1h-3v1l-1-1h-2v1l-1-1h-1v2l-1-1-1 2-1-2h1v-1l-3-1h-1l2 1h-1l1 1-2-1h-1 1-1l1 1h-1 1-2l1 1h-2 6l-2 1h2-5l3 1 1 1 1-1-1 1h1l-3 1h4l5 2 7-3h1 1v-1l2-1h-1l1-1h-2l1-1h-1v-1h-1z')
  static Path get _is => _$MapSvgData__is;
  @SvgPath('m761 225v-1 2-1zm-1-1zm0 0h1v-1l-1 1zm0-2h-1 1zm0 0zm0 0v-1 1zm-57-1zm56 0zm1-1zm-2-2zm-54-3zm-3-1zm3-1zm-4 0zm57 1v-1h-1l1 1zm-57-2zm2 0h-1 1zm-2 0zm2 0v-1 1zm0-1zm55 0zm-1 0v-1 1zm-55-1zm57-1zm0 0zm-1-1-1 2 1 1v-3zm1 1v-1 1zm2-1zm-3 0zm0 0v1-1zm0 0zm0-2v2-1-1zm0 1v-1 1zm0-2zm4 0v-1 1zm-4 0v-1 2-1zm0-1zm-34-7zm4-3zm16-15zm-45 0 1-1h-1v1zm43 0v-1h-1l1 1zm1-1zm-2 0h1-1zm-35-43h-1l-2 1v1h-8l1 2h1l-1 1 1 2 4 3-2 1 1 3-2 2 1 1h-2l-3 6-2 1-2-1-1 3v1l1 1v2l2 1 2 4h-3-3v1l-2 1h1 1l-1 1 2 2 4-1-1 2h-3l5 5 4-1v-4l2 1h-1v1h1l-1 1 1 2v3l1 2h-1v1h1v1l2 8 3 5 2 5 3 5 3 8 2 2h2l1-3h3l-2-1 1-3h1v-4l1-5-1-1h1l-1-5v-2h2v-1 1l1-2h3v-2l4-4 3-5v1l3-1 1-2v-3l2-1 1-1-1-1 1 1v2l1-1v1-1-1 2h1v-1-1l-1-3-1-2v-2l-2-1 1-2h1l-2-2v-2l3 2 1-1 1 3 7 1-1 2-2 1v1l2 2v-2h1l2 5h1v-6h2l1-3v-1l1-3-1-1 3-2h2l-1-1 1-2-3-1 1-1-1-1h-1l1-1h-1l-2 1-2-1-5 5h-2l2 2v1h-7-2v-2l-1-2-2 1 1 5-6-1-5-3h-2l-4-1-6-3v-3l2-2-6-4-1 1-2-4h1l1 1 1-2-3-2v-3l-2-1-1-2zm31 49z')
  static Path get _in => _$MapSvgData__in;
  @SvgPath('m646 163h-1 1zm-2-1h-1 1zm8 0zm-2 0zm2 0h-2l-1 1 3-1zm0-1zm-17-6v-1h-1l1 1zm-20-33v-1h-1v1h-1l2 3-1 2h1l3 6 3 1h-1l1 2h-1l-1 3 3 3v1l3 1 2 2v3h1v1l1 2h1v-2l2 2 1-1 5 8h3l4 3 3 1 3-2h2l2 4 12 2v-3l3-1 1-2h-2v-4h-3l-3-5 2-3-1-1-2-1-2-5 1-1-2-2 1-1v-4l-1-3h-2l-3-3-5-1-1-1h-5l-1 2-2 1v1l-7 1-3-3h-3l-1-3-2-1v-2l-1-1-3 3h-1l-4-2z')
  static Path get ir => _$MapSvgData_ir;

  @SvgPath('m529 99h-4v1h-1v1l-2-1v2l-2-2-1 2h-2-1l1 2h-1l1 1v2h1v1l3-2 4 2 2 5 12 7 2 4-1 2 1 1 1-3 2-1-2-2 1-2h2l1 2 1-1-2-2-5-2v-2h-3l-2-2-2-3-3-2v-1-1l-1-1 1-1 2-1 1 1h1l-1-1v-1h-1l1-1-3-1h-1zm1 9h1-1zm1 6zm-1-11v1-1zm-6 8zm2 0-1 1h1v-1zm-1 1zm2 1zm-2 0zm-2 4h-3l1 4v2l1 1v-2l2 1v-5l-1-1zm-3 0zm12 0zm3 1zm1 0zm-15 5h-1 1zm17 1h1-1zm-5 0zm5 1h-1 1zm0 0zm0 0zm1 1-4 1-1-1-3 1 8 4v-1l-1-2 1-2zm-9 1zm1 0zm-1 4 1-1h-1v1z')
  static Path get it => _$MapSvgData_it;
  @SvgPath('m490 231zm-4 0 2-1h-1l-1 1zm0 0 2-1 2 1v-1l1 1v-2l-1-3 2-5v-4l-3-1h-3l-2-2-2 1v-2l-2 2-2-1-1 1-1 2 1 1v1l1 2-2-1 1 2-2 1 1 2-1 1 3 3v4l7-2-1-1 2 1z')
  static Path get ci => _$MapSvgData_ci;
  @SvgPath('m620 136h1l-1-2h1l-3-1-2-4-1 1-1-1-3-1-4 4v6l-5 3 2 4 3 1 5 2 7 7h5l2-3h2 1l-1-2v-1h-1v-3l-2-2-3-1v-1l-3-3 1-3z')
  static Path get iq => _$MapSvgData_iq;
  @SvgPath('m919 170zm-82 0h-1 1zm-3 0zm23 0v-1 1zm-19 0v-1l-1 1h1zm46-1zm-43 0-1-1v1h1zm42-2v-1 1zm-27-2zm-13-2v1-1zm42 0v-1 1zm-38 0v-1l-1 1v1l1-1zm38-2h-1 1zm-1 0zm-3 0zm-34-1zm37 0zm-1-1zm-35 0zm1-1h-1 1zm1-1zm-1 0-1 1h1v-1zm-1-4zm0-1v1-1zm0 0zm2-1h-1 1zm26 0v-1l-1 1h1zm-26 0v-2 2zm-4-4v-1 1zm27-1zm-27-1zm1 0v-1h-1l1 1zm-1 0v-1 1zm-4-1-1-1 1 1zm29-2h-1 1zm-29 1v-1 1zm1-1v-1 1zm7-2zm-7 0zm26 0zm-19 0h-1 1zm-3 1-2-1-2 1 2 2-1-1 1 2h1l-1-2 2 1-1 1h1l-1 2 2 2h1l-1-1h1v2l1-1v-5l-1-1h-1v-2l-1 1zm-3 0zm25-2h-1 1zm-19 0zm0 0v-1l-1 1h1zm1 0v-1 1zm-10 0v-1 1zm13-1-1 1h-2l-1 2 3 2 1-2 2 1v-2l-2-2zm14 0h-1 1zm-14 0zm2 1v-2l-1 1 1 1zm-15-1v-1 1zm27-1h-1 1zm-19-4zm0 0zm-1 0h1-1zm1-1zm8-3zm3-3-1-1v2l1-1zm1-10-1-1v1h1l1 1-1 1-2-2v2l1 1v1l1 1v1l1 4-1 2-2 3-2-2 1-1-2 1 1 2-1 2 1 1h-8l-2 4h-2l1 2 2-1 1 1v-2l8-1v3l2 1 1-2h1l-1-2v-1l1 1h1l-1 1 3-1 1-1 1 1v-2l1 1v-2l1 2h1l1-2-2-2v-2l-2-4h1l-1-4-5-6zm-6-3v1-1zm12-4h-1 1zm-5-2zm-9-3zm2-1-1 1 2 2 1 4h-2l1 1-1 1 3 4 2-1-3-2-1-1 2 1 2-1 5 2v-3l3-1h-1l-1-2-1-1v1l-3-1-7-4zm-3 1v-1 1z')
  static Path get jp => _$MapSvgData_jp;

  @SvgPath('m614 254zm-15-22h-4l-1 2 1 1 2 6-3 6v3l10 7v1l5 4 3-6 1-1v-1h1l-1-1h2v-1l-1-2v-12l2-4-2 1-1-2-3 3h-4l-3-3-4-1z')
  static Path get ke => _$MapSvgData_ke;
  @SvgPath('m704 113-4-2h-9l-4-1-1 1v2l-5-2-1 1v1h1l-2 2 3 2v-2l2 2 3 1-3 2-2-1-1 1h-3v2h5l1-1 2 2 4-1v-2l2-2h1l1 1h2v-2h3l5-4z')
  static Path get kg => _$MapSvgData_kg;
  @SvgPath('m822 122v-1 1zm10-9-3-2v2l-1 1h-2l1 2-4-1v2l-3 3 1 1 2 1v-1l1 3h1l-1 1v1h1v1h1v1l1-1-1-1 3 1 1-1 2-1v-1l-3-1-1-2 4-3-1-3 1-2z')
  static Path get kp => _$MapSvgData_kp;
  @SvgPath('m77 283zm-3-4zm6 0zm0-1zm-17-13zm-45-4h-1 1zm2 0zm46-1h-1 1zm-46-3zm-1-1zm975 0v-1 1zm-3-1zm-1-2zm0 0zm-1-1zm-1 0zm5 0v-1 1zm-1-1h-1 1zm-4 0zm-1-1v-1 1zm0-1zm-2-2v-1 1zm-1-1zm0 0zm2 0zm0 0zm-3-2h1-1zm0-1zm1 0zm-1 0v-1 1zm0-1zm0 0zm0-1zm0 1v-1 1zm0-1zm1 0zm0 0zm-926 0 1 1-1-1zm925-4h-1v1l1-1zm0 0zm-930-2h1-1zm-2-3z')
  static Path get ki => _$MapSvgData_ki;
  @SvgPath('m835 142 1-1-2 1h1zm0-4h-1 1zm1 0h-1 1zm1 0zm-4 0zm4 0v-1 1zm-5-1zm0 0zm5 0v-1 1zm-5 0v-1 1zm5 0v-1 1zm2 0h-1 1zm-7-1zm-1-4-1-1v1h1zm-2-2zm12-2zm-12 0v-1 1zm-5-1zm9-2-1-1v1l-2 1-1 1 2 2h-1l1 1h-1l-1 1h1l2 2v1h1v2h-1l2 1h-1l1 1v-1l1 1v-1 1h1v-1-1h4v-1l-2-5-5-5z')
  static Path get kr => _$MapSvgData_kr;

  @SvgPath('m625 105v-1l-1 1h1zm0 0v-1 1zm6-4v-1 1zm29-28-7 2-1 1-4 1h-1-4l1 1h-1l2 1h-1l3 1h-3v1l1 1-2 1 5 2-1 2h-3l-1 1-3-2-2 1-3-1-1 2-3-2v2-1l-4-2h-2v-1l-3 1-1-1-4 3 1 2-2 1-2-2-1 2v1l1 1-1 2 2 1 1 1h2l3 3-1 1 1 1 5-3 5 1v1 1 1 1l1 1h-4l-1 1 2 1h-4l2 1 2 3 4 2v3l1-2h3l4 3h1l-3-11 6-2 11 6h7l3 2h1l1 3h1l1 2h3l2 2 1-2 4-3v-1l1-1 5 2v-2l1-1 4 1h9l4 2-1-1 1-1h-1l1-1-2-3-2-2 4-1h2v-1h-1v-5l4 1 2-1-1-4h2l-1-2h2l-3-1v-1l-1 1-4-2h-1l-4-3-4 1-3-2v2l-9-8-5-2v-1l-4 3h-2l-1-1 1-1h-3l-2-1 1 1h-2l-1-1v-1l-2-2h-4z')
  static Path get kz => _$MapSvgData_kz;
  @SvgPath('m797 200v-1l-1-2v-1l-2-1-1-2-4-4v-1l-4-2v-1h2l1-1-3-3-1 1-2-1-1-3-2-2h-1l-1 1 1 3h-1l-1-1-2 4h1l1 2h2v6 1l3-3 1 2 2-2h2l2 3 1 3 2 2 1 4-1 1 2 1v-1l1-1 1 1 2-2z')
  static Path get la => _$MapSvgData_la;

  @SvgPath('m561 72v1h1l-3 1v2l1 1h-6l2 4-2 1 2 1v2l1-2h4l12 2v-2h2l-2-3 4-1-3-1v-1l-3-2 1-1-1-2-7-1-3 1z')
  static Path get by => _$MapSvgData_by;

  @SvgPath('m526 159v4l-1 1 2 5 4 1 1 3 4 1 2 1 2-1 3-1 22 12v-1h3v-7l-2-23-1-3 1-4v-1l-5-1v-1l-4-1-4 2v4l-3 2-4-2-5-1-2-4-9-2v2l-3 3-1 3-2 2 1 2 1 5z')
  static Path get ly => _$MapSvgData_ly;
  @SvgPath('m637 301v-1 1zm-7-5zm3-6v-1 1zm3-5-2 1 1 2-1 2-1 1-1-1v2l-1 1 1 1v-1l-2 2 1-1-2 1 1 1h-1v-1l-2 2 1 1-1-1-5 1-2 4 1 8-3 5-1 3 2 8 3 2 5-2 10-26-1-4h1l1 2 1-2-1-8-2-3zm1 2z')
  static Path get mg => _$MapSvgData_mg;

  @SvgPath('m725 100 1 3 1 1 7 1 5 2 5 5h11l13 3 5-3 4 1 3-1 2-3-2-2v-3l5 1 1-2h2l1-2 2-1h6l-2-2-4-3h-2v1h-5l-1-6-7-2-1 2-5 2-6-1-2-1-1-1-3-1h-10l-3-3-9-2-1 3 2 2-1 2-7-1-2-2h-5l-8 5 1 2 2 1 4 1 4 4z')
  static Path get mn => _$MapSvgData_mn;

  @SvgPath('m475 211h1l-1 1h1l1 3 1-1 2 1 2-2v2l2-1v-4l3-2v-3l1-1 1 1 1-1h1l1-2h1l1-1 3-2 3 1 3-1 6-1 2-3v-7-2l-3 1v-3l-4-1-2-2v-1l-16-12h-5l3 27v3h-11v-1 1h-4v1l-2-2-1 3h-1l1 4 1 2v2l1 1 1-1v1l1-1 2 1 2-1 1 3z')
  static Path get ml => _$MapSvgData_ml;
  @SvgPath('m475 143-2 4v4l-1 3-4 3-5 2h13v-3l4-3 4-1 2-2 3-1v-2l2-2h5v-1l-1-2-1-5-1-1-2-1h-4l-2-2-2 1-2 5-6 4z')
  static Path get ma => _$MapSvgData_ma;

  @SvgPath('m454 185v-1 1zm27-21-5-3-1 4h-9v8l-3 2v4h-7-3l-1 2 1-1 1 3v3l1 5-2 5 1-1 5-1 6 6h1l1-3 2 2v-1h4v-1 1h11v-3l-3-27h5l-5-4z')
  static Path get mr => _$MapSvgData_mr;
 
  @SvgPath('m653 192zm1 0zm7-10v-1l-1 2 1-1zm-9-14-1 1 1 2h-1l-1 4 2 2-2 7-8 3 4 7 5-1 1-2 2-1 2-2 2-1v-4l1-1h1l3-5-4-5-4-1-2-3-1 1v-1zm0-5-1 1 1 2v-3z')
  static Path get om => _$MapSvgData_om;
  @SvgPath('m704 249zm0 0zm0 0zm0 0zm0 0zm0 0zm1-1zm-1-2zm0 0zm-1 0zm1 0zm1 0zm-2 0v-1 1zm2-1zm-1-4zm0 0zm0 0zm1 0zm-1 0zm1 0zm0 0zm-1 0zm1-1zm-1 0zm-1 0zm1 0v-1 1zm0-1zm-1 0zm0-1zm1 0zm-1 0zm2 0zm-2-1zm2-1zm0 0zm-2 0zm0 0zm-1-1zm3 0h-1 1zm-3-1zm3 0zm0 0zm0 0zm-2 0zm0-2zm2-2zm-1 0zm1 0zm-1-1zm-1 0zm1 0zm-1 0v-1 1zm1-1zm0 0zm-1 0zm1-1v1-1zm0 0zm0 0h-1 1zm-1 0zm0-1zm0 0zm0 0zm0-1zm0 0zm0 0zm0 0zm0 0z')
  static Path get mv => _$MapSvgData_mv;
  @SvgPath('m183 189h-1 1zm75 0v-1 1zm1-1zm-13 0h1-1zm-53 0v-1 1zm1-2zm67-4h-1v1l1-1zm0-2zm-53 0v-1 1zm-1-1zm0 0v-1 1zm0-1zm24-3zm1 0h-1 1zm-33-4v-1 1zm-4-1h-1 1zm-1 0v-1 1zm4 0v-1 1zm0-2zm-4 1-1-1 1-1-1 1 1 1zm9-3h-1l1 1v-1zm-5 0zm-1-1v-1 1zm3-4v-1h-1l1 1zm-10-2zm-2-1v-1l-1 1h1zm7-2h-1 1zm0 0zm-15 0v-1 1zm16 0v-2l-1 2h1zm-2-1-1-2v1l1 1zm-1-8-1-1v1h1zm4-1-4-2 1-1-7 1 1 9 3 4-1 2h1-3l3 3h1l2 2v2 3l4 3v3l2-2-2-3v1l-1-1-1-8v1l-2-6-3-5 1-6 4 2 1 7 2 4h1v1l3 4-1 1h1l-1 1h1l2 2v1l4 6 2 4-2 2 1 1h-1l1 4 4 3 4 1 2 2 8 4 4 1 5-2v1-1 1h2l-1-1 4 4 1 2 2-5h4v-1l-3-3h2v-1h5l2-3h1v-1l1 2 1-4 3-5-1-1h-4l-5 1-1 4-3 5h-1v-1l-8 2v-2l-3-1-3-5-1-7 4-10-5-2v-3l-4-7-2-1-4 3-2-2v-3l-3-4h-5l-1 2h-7l-5-2zm4 15zm30 17z')
  static Path get mx => _$MapSvgData_mx;
  @SvgPath('m790 240zm21 0v-1-1l-1 1 1 1zm-21-2h1-1zm38-4h1-1zm3-1zm-1 0zm1-1h-1 1zm-10-2zm-42 0zm43-1zm7 0zm-6-1zm-1-1zm-44 0h-1l1 1v-1zm2 0-1-1v1l2 8 1 2v1l7 5 1-1 1 1-3-5v-6l-4-5v2l-1-1-1 1-1-2h-1zm47-1zm-2 0v-1l-2 4-2 1 1 1-1 1 1 1-1-1-1 2-2-1-3 4-4 2v1l-1-1v1h1l-1 2 1 1-3-2-1 1-1-1v1l2 2h4l2-2 3 1h3v-3l1-1v-1l1-1 1-3h1 4l3-1-2-1 3-2-2-1h-1v-1h-2l1-1-2-2-1 1zm1-2v1-1zm-1 0h1-1z')
  static Path get my => _$MapSvgData_my;
  @SvgPath('m589 330v-1l-1 1h1zm7-14v-1 1zm13-17zm3-17v-2l-4 2-3 1-2 1h-3l-1-1-3 1h-1v2 3l4 5-1 4-2 2 1 1-3-4 1-1v-3h-3l-1-2-8 4v2 1h3l4 2v2 3l-1 2 1 3-2 2v3l-3 3 1 7v4 3h2l1-2h-1v-1-1l6-3 2-2v-5l-2-9 1 1 4-3 2-3 6-3 4-5 1-2-1-6h1v-6zm-17 3zm1 0z')
  static Path get mz => _$MapSvgData_mz;
  @SvgPath('m596 285zm-1 0zm-4-8 2 4-1 1v4l-1 1-1 3 1 1 1 2h3v3l-1 1 3 4-1-1 2-2 1-4-4-5v-3-2h1l-1-6-4-1z')
  static Path get mw => _$MapSvgData_mw;
  @SvgPath('m957 319zm0 0h1-1zm-1-1zm12 0zm-14-1zm1-1zm5-1h-1l1 1v-1zm0-1h-1 1zm-4-1zm2 0v1-1zm-1-1-1 1 1-1zm-6-1h-1 1zm-1 0zm1 0h-1l2 4 3 3h1l-5-7zm-1 0zm-1 0zm1 0v-1 1zm-1-1v-1 1zm0-1zm-10-1z')
  static Path get nc => _$MapSvgData_nc;

  @SvgPath('m511 188v7l-2 3-6 1-3 1 1 4 2 1h-1v1l1 1h2l1 2v-1h1l2 2 2-6 4-1 2 1 1 2 3-1 5 2 3-2 5 1 3-2-1-3 6-8 1-10-2-4-1-5-2 1-2-1-4-1-13 8-4 4-4 1v2z')
  static Path get ne => _$MapSvgData_ne;

  @SvgPath('m902 202zm0-2h1-1zm1 0v-1l-1 1h1zm0-1zm-1-4zm0-1zm0-2zm0-1zm-0.31262 0-0.68738-1v-1 1zm-1.6874-3m0.4245-0.25677-0.4245 0.25677zm-1.4245-2.7432zm-1-1zm-1-2z')
  static Path get mp => _$MapSvgData_mp;


  @SvgPath('m669 404zm-2-1zm2-1zm-1 0 1-1-1 1zm0 0 1-1-1 1zm1-1zm-1 0-2 3 3-1 1 1-1-1h2v-1l-2 1-1-1h1-1v-1zm1 2zm1 1-1-1 1 1zm-3-3zm1 0v-1 1zm-39-7zm-1 0zm-4-1zm76-23zm1-3zm-84-66z')
  static Path get tf => _$MapSvgData_tf;




  @SvgPath('m966 311zm1-2zm-2 0v-1 1zm1-1zm-1-1v-1 1zm-1-4h-1 1zm0-3zm0-1zm0-1v1h1l-1-1zm-2 0v-1 2h1l-1-1zm0-1zm0-1zm0 1v-1 1zm3 1v-1-1 2zm-1-2h-1 1zm-2-1zm0 0v-1 1zm3 1v-1-1 2zm-4-1v-2 4-1l1 1v-1-2l-1 1zm3-3h-1 1zm0-1zm1-1h-1 1zm-1 0zm-2 0zm0-1zm0 0zm0 0v-1 1z')
  static Path get vu => _$MapSvgData_vu;
  @SvgPath('m520 233zm-1 0zm-4-30-4 1-2 6 1 4-2 4h-1v9l3-1-2 1h4l1 2h1v1h1-1v1l2 3 2-1v-1 1-1l1 1-1-1 1 1v-1l1 1 3-1 1-3 2-3 3-1 2 2 1-2 1-5 2-1 2-7 3-2v-2h-2v-2l-1-2-3 2-5-1-3 2-5-2-3 1-1-2-2-1z')
  static Path get ng => _$MapSvgData_ng;
  @SvgPath('m508 85h1-2 1zm5-3v-1h-1l-1 1h1 1zm-1-1h-1v1l1-1zm-2-1h1-1zm1-1zm4 0-5 1v2h1v-1h1l-1-1h1l1 1h-1 1v1h-1-2l-2 2h1-1l1 1h-2 2 2l2 1v1h1l-1-1 1-1-1-1 2-1 1-1-1-1h1v-2h-1zm-4 0h1-1zm2 0h-1 1zm1 0z')
  static Path get nl => _$MapSvgData_nl;
  @SvgPath('m519 64zm-7-2zm0 0zm0 0v-1h1l-1 1zm-1 0v-1 1zm0-2v1l-1-1h1zm0 0v-1 1zm0 0v-1h1l-1 1zm0-1zm1 0zm-1 0zm-1 0v-1 1zm1-1h-1 1zm0 0h-1 1zm0 0 1-1-1 1zm-1 0v-1 1zm-1-2zm1 0zm0-2zm0 0zm1-1h1-1zm1 0v-1 1zm1-1h-1 1zm1 0h-1 1zm0-1zm1 0h1-1zm1 0zm0-1h1-1zm1 0h1-1zm0 0h1-1zm0 0h-1v-1l1 1zm0 0v-1h2l-2 1zm1-2v1h-1l1-1zm5-2zm0 0zm0-1-1 1 1-1zm0 0h-1 1zm1 0zm1 0h-1 1zm0 0v-2 1 1zm-1-2zm1 0v-1h1l-1 1zm0-1h1-1zm1 0zm0 0h1-1zm3-3v1l-1-1h1zm0 0h-1l1-1v1zm1-2h1-1zm2 0h-1 1zm-6 0zm1 0h-1v-1l1 1zm1-1-1 1 1-1zm2 0h-1 1zm-2 0zm0 0h-1 1zm2 0h-2 2zm3 0h-1v-1l1 1zm1-1h-1 1zm0 0v-1l1 1h-1zm-5 0v-1 1zm3 0h-1 1 1l-3 1 1-1v-1l1 1zm1-1h-1 1zm-3 1h-2 2v-1 1zm4-1h1-1zm-3 0 1-1-1 1zm4-1h1v1h-3 1l-1-1h2zm25-1zm-23 0-2 1 2-1zm4 0h-1 1zm18 0h1-1zm-16 0zm-5 0h1-1zm1-1zm-1 0 1 1h-2l1-1zm3 0v1-1zm0 0zm-4 0v1-1zm4 0zm-3 0zm3 0h-1 1zm-2 0h-1 1zm-1 0h-1 1zm7 0h-1 1zm-2 0-1-1 1 1zm3-1v1l-1-1h1zm1 0h-1 1zm-1-1-1 1h-2l3-1zm1 0h-1 1zm1 0zm2 0h1-2 1zm-39 26-1-1h4-4 1l-1-1h3-1 1l1-1v1h1l-1-1h-1 1-1 4-3v-1l4 1-2-1h2l-1-1h1-1 1v-1l1 1v-1h1l1 1 2-1h-1l2-1h-1 1-1-1 1v1h-2-1 1-1l1-1h-1l2-2 1 1 1-1h-1l2-1h-2 1 1 1v-1h-1 1l-1-1h1 1v-1h-1l3-1-3 1 1-1h-1 2l-1-1h1-1l5-1h-3l1-1 1 1v-1h1l-3-1h3-2l2-1v1h1-1 1l-1-1h2-2 3-1l1-1h-2 2-1 1v-1h1v-1l2 1-1-1 2-1v1l1-1-1 1h2l-1-1 2-1 2 1-1-1h-1 3-1l2 1v-2h2l-1-1h1 3l-2 1h1l-1 1h1l2-2v2l2-2h-1 1 2l-2 1h1 1l-1 1 2-2 4 2h-5l3 1h-1 1 2l1 1-2-1-2 2h-1l1-2h-2-4-1v2l-1 1h-2-3l-3-2-1 1h-1l1 1-1 1-4-1v2h-3l-1 1 1 2-2 1v1h-2l1 2-2 2 1 1v1h-2l-2 1v1l1 3v1l1 1-1 1 1 2v1l-2 1 1 2h-1-2v-2 1l-1-1 1 1-1 1h-1l-1 1h-1v1h-1v1h-4v-1l-3-1 1-1 1 1 1-1h-2l2-1h-3l2-1h-1l1-2h1v1l1-1h-2l-1 1v-1l-1 1-1-1h2v-1l-1 1-1-1 1 1v-1l-1-1h1 1 1 1l1 1-1-1h1l1-1h-1v1h-1v-1l-1 1h-1-1-1l1-1h-1 2-2 1-1 1zm1 7z')
  static Path get no => _$MapSvgData_no;
  @SvgPath('m721 159 4 1h2l5 3 6 1-1-5h-2l-2-1v1l-1-1v1l-1-2h-2v-1h-2l-1-2-2 1-5-4h-1l-1 1v-1l-2 2v3l6 3z')
  static Path get np => _$MapSvgData_np;

//================= fast===============================
  @SvgPath('m266 209zm2 0v-1 1zm0-1zm1-6 1-1-1 1zm-1-2-3 1-1-1-3 3-1-1-2 1v2l-2 1h-1l5 6h2l3 1 1-1v-3l1-3v2-4l1-2v-2h1-1z')
  static Path get ni => _$MapSvgData_ni;
  @SvgPath('m902 413 1-1-1 1zm-2-5zm1-1zm0 0h-1 1zm1-1zm32-2h1-1zm-25-5zm32-1zm-28-2h1-1zm2-1zm1 0-3 1 3-1zm-1-3 1-1-1 1zm2-2zm-862-3zm-2-1h-1 1l1 1-1-1zm893-8zm-1-1 1-1-1 1zm-1-1-3 1-1 1-7 5-9 3-5 3-2 2v-1l-1 1h1-1v1l-2 1 5 2 6-3 5-5 5-1h1l1-2 8-5 1-1h-1l1-1-3 1 1-1-1-1h1zm16-9zm-1 0v-1 1zm-1-3h-1 1zm2-1zm-2 0v-1 1zm1-1zm1 0v-1l-1 1h1zm-3-6h-1l-1 3 1 1 1-1-1 1v3-2l1 1h-1v1 2h1-1l-1 2-3 3-3 2 2 2-2 2-3 2 1 1 8-4 3-3h2l5-5-1-1-4 2-2-2 2-2-1-1-1 2-1-1 2-5-2-2-1 1 1-2zm-2-1zm-935-11zm0-4z')
  static Path get nz => _$MapSvgData_nz;
 
  @SvgPath('m279 262-1 1-2-2h-2v-1l1-1-1-1-2 3 1 4-1 1 4 3 3 4 4 10 4 7v1l1 3 3 2 11 6 3 3h1l1-2v-1l2-3-2-2v-2-1l1-2-1-4 1-1-3-5h-3v-5l-2 2h-2l-1-2h-2l1-1-4-5 1-2 2-1v-5l6-3h2l-2-1 2-3-1-1-4-1-3 1-2-4-2-1-1-2-2-1h-1l1 3v2l-3 3-5 3-1 4z')
  static Path get pe => _$MapSvgData_pe;
  @SvgPath('m683 171zm12-41h-1-5l-3 3 2 2-1 3v1h-3l2 2-2 1v4l-1 1-2-1-1 1v1h-2l-1 1v3l-10 2-4-2 3 5h3v4h2l-1 2-3 1v3l6-1 2 1h5l-1-1h1l1 1v1l1 1v1l1 1 2 1 2-1v-1h3 3l-2-4-2-1v-2l-1-1v-1l1-3 2 1 2-1 3-6h2l-1-1 2-2-1-3 2-1-4-3-1-2 1-1h-1l-1-2h8v-1l2-1-5-1-1-3-3-1z')
  static Path get pk => _$MapSvgData_pk;
  @SvgPath('m545 91 1-1 2 2 4-1 2 1h1l-1-1 3-4-1-2v-2l-2-1 2-1-2-4-1-1h-7-2 1-2l-1-1h1-1-1l-8 2 1 1h-1v3h1v3l1 1v2h4l-1 1 1 1h1v-1h2v1h2l1 2z')
  static Path get pl => _$MapSvgData_pl;
  @SvgPath('m271 224zm1-1h1-1zm-1 1v-1h-1l1 1zm2-1v-1 1zm-1-1zm-3-1zm1 0h-1 1zm-1 0zm9 0zm0 0v-1 1zm1 0v-1 1zm-9-3zm0 0v-1 1zm0-1zm0 1v-1 1zm8-1-5 2-2-1v1h-1l-1-2h-1v2 3-1h1 1 1l1 2v-1h1v2h1l2-1-2-2 5-3 1 3 1-1 1 1-1-1v1l-1 1 2 2v-1l2-1-1-2-2-2-3-1zm3 4z')
  static Path get pa => _$MapSvgData_pa;
  @SvgPath('m453 143 1 1 1-1h-2zm3-1h-1 1zm-22-12h-1 1zm-2-3h1-1zm-6-2h-1 1zm3-1h-1 1zm-11-2zm60-8v-1l-1 1v4l-3 6 2-1-1 2h1l-1 5 5-1v-2l1-1-1-1 1-2-1-2h1v-4l2-2-1-1h-4z')
  static Path get pt => _$MapSvgData_pt;
  @SvgPath('m927 284-2-1 1 1h1zm1-1zm-2 0h-1 1zm-1-2h-1 1zm-5 0h-1 1zm-1 0v-1 1zm1-2-1-1 1 1h1-1zm-1-2h-1l2 1-1-1zm-1 0v-1l-1 1h1zm7-1h-1 2-1zm-4-2h-1l1 1v-1zm-21 1-1-1h-1l2 1zm0-1h-1 1zm0 0v-1 1zm0-1zm0 0zm0 0v-1 1zm4-1zm-3 0zm31-7-1 1 3 3-2-4zm-19 0-1-1v1h1zm0-1zm-1 0h-1 1zm-2 0zm21 0v-1 1zm-15-1v-1 1zm-8-1zm9 0zm28 0v-1 1zm-38 0v-1 1zm23-1zm-5-1h-2v3l-4 2v-2l-1 2-4-1-1 1 2 2h4l3-2 2-1v-1l1-1v-2zm-10 6zm-10-6zm24 0zm-1-2zm4 0zm-4 0zm-2-1zm-5-1v1l1-1h-1zm3 1 1-1h-1v1zm0-1zm-3-1v1l4 2 1 2v2h1v-2l-3-3-3-2zm-25 1-2-1-2 21 5 1 2-1-1-2-2-1h4l-1-1h2l-1-1 2 1v-1l5 2 1 3h1v1l2 2 5 1 1 1h2l-1-1 1-1h-2l-1-1h1l-2-1v-1h-2l-1-3-2-2-1-3h3l-1-2-5-2v-2l-3-3-7-3zm28 0v-1 1zm-4-1h-1 1zm-7 0v-1 1zm-2-2h-2v1l2-1zm-12-1v1-1zm19 0zm-14-1h-1 1zm0 0z')
  static Path get pg => _$MapSvgData_pg;
  @SvgPath('m455 212zm-1 0h1-1zm0 0zm1 0v-1 1zm0 0v-1 1zm-1 0v-1 1zm1-1zm-1 0zm0 0-1-1v1h1zm1 0v-1l-1 1h1zm1 0v-1 1zm-2-1 1-1h-1v1zm0 0v-1 1zm5-3-6 1h1v1h1v1l2-1v1h-1 1l-1 1h1l-1 1h1l1-1 3-1-1-1 1-2h-2z')

  static Path get md => _$MapSvgData_md;
  @SvgPath('m833 232zm2-1h-1 1zm-1 0zm1-1zm0 0-1 1 1-1zm15 0h-1 1zm0 0zm-13 0v-1 1zm1-1zm0-1h-1 1zm1 0h1-1zm-3-1zm4-1h-1l1 1v-1zm-10-1zm20 0v-1 1zm-24-3v-1l-1 1h1zm0-1zm25-2zm0 0zm-7-1h-1 1zm3 0zm-3-1zm6 0v-1 1zm0 1-1-2v3h-2v1h-1l-2 2v-2l-1-1-1 2-2 1-1 3h1l1-3 1 2 1-1 1 1v-2l2 2-1 1 1 2 2 2 1-1v1l1-1-1-2 1-2 2 3-1-2 1-1-2-6zm-4-2zm-3 0zm7 0v-1 1zm-4-1h-1l-1 1 2 1v-2zm2 1v-1 1zm1 0v-2 1 1zm-16-2zm12-1v1-1zm1 0zm-5 1v-1l-1 1h1zm8-1zm-6 0h-2v2h-1l2 3h1l-1-2 1-3zm-7 0zm8-1-1 5 1-3v-2zm-1 0v-1 1zm-11-1v2l-1-1v2l-5 7 6-7v-3zm0 0zm1 0h-1 1zm13 0-1-1 2 5v-1h1l-1-2v-1h-1zm0 0-1-1 1 1zm0 0v-1 1zm-7-1h-1 1v4l3-2v-2l-1 1-2-1zm-6 0v-1h-1l1 1zm0 0zm1 0-1-1 1 1zm-1-1zm0 0zm3 0v-1 1zm-3 0-1-1 2 1h-1zm7-1h-1 1zm5-1h-1l3 3-1 1h1l1 1h1l-2-4-2-1zm4 5zm-6-4-1-1v2l1-1 1 2-1-2zm-4 0v-1 1zm4 0v-1h-1l1 1zm-5 0v-1 1zm4-1-1-2v1l1 1zm2-2zm0 0h-1 1zm-1 0zm-5 0zm0 0-1-1 1 1zm-4-1h-1l3 4 1-1v-2l-3-1zm-1-1h-1l1 1v-1zm11 0-1 1h1v-1zm0 0zm0 0zm-6 0v-1h-1l1 1zm0-3h-1 1zm-1 0v1-1zm-6-4v-1 1zm2-8h-2l1 8h-1v-1h-1l2 5 1 1v-1l1 1v1 1l2 1 1-1 3 2-1-2 3 3h2l-1 1h1v-1h-1l-1-2 1-1-2-1v2l-2-2h-1v1l-1-1-2-4 3-5-2-2v-3l-1 1-2-1zm7 15zm-7-16h1-1zm2 0zm-2 0v-1 1zm1-1h-1 1zm1-1h-1 1zm-1-2zm0 0v-1 1zm0-1v-1 1zm0-2z')
  static Path get ph => _$MapSvgData_ph;

  @SvgPath('m834 111h-1 1zm35-2zm1 0-1-1h1v1zm-250-1h-1 1zm247-2h1l-1 3v-3zm5-2-3 2v-3l3 1zm-253-1zm1 0v-1 1zm1 0-1-1 1 1zm253-1-1 1 1-2v1zm0-2zm-1-1zm3-1v1h-1l1-1zm0 0v-1 1zm1-2h-1 1zm-1-1zm0-2h1-1zm0 0v-1 1zm0-1h1-1zm1 0-1-1v-1l1 2zm-2-2zm2-1h-1v-2l1 2zm0-2h-1 1zm-2 0-1-1h1v1zm-25 4h-1 1l-1 1 1 3 4 4h1l2 2-4-2 1 3-3-2-2-5-2-2-5-7-5-4-2-3 1-1-2-2 5 4 4 5 11 8-4-2zm-27-15zm-1 0v-1 1zm-277 0 1-1-1 1zm347-1 2 1-3-2 1 1zm-73-1h1-1zm3 0v1h-2v-1h2zm-273-1 3 1 1 2h-7l1-1h-1v-1l2-1-1 1h2v-1zm337 0 4 2-4-2zm-16-11-1 2-1-2h2zm-31 0h-1 1zm-4 0zm14-1zm-286-2v-1 1zm13-12zm-445-1h-1 1zm454 0h-1 1zm-454 0h1-1zm444-1h-1 1zm1 0h-1 1zm-434-2h1-1zm502-2v-1 1zm1-1zm-57 0zm55 0h2l1 1h-1l-1-1h-1zm-33-4zm0 0h-1 1zm-7 0h1-1zm14-1h-1 1zm20 0h-1 1zm-25-1zm-479 0 4 4h-1l-1 1h2l-3 1h-1v1h2l1-1h1l-1-1h2 1l-1 1h3v2h1l-2 1h-2-1v1l-3-1 1 1h-2v1l-3 1v1h-2l1-1-2 1-2-1 1-2h-5l2-2 2-1-3 1-2 1v1l-4 1 17-11zm477 0zm24 0h1-2 1zm-15 0-1-1 1 1zm-50-1h-1 1zm66 0zm-34 1-2 1-2-1v-1h1l3 1zm227 0h-1l-2-1h1v1h1 1zm-193-1zm191 0h-1 1zm16-1zm-1 0-1 1-3-1h4zm-175-1h-1 1zm-48 0 3 1h-2l-3-1 1-1 1 1zm48-1v1-1zm-53 0zm210 0zm-158 0v-1 1zm156 0v-1 1zm-2-1h-1 1zm-214 0v-1 1zm0-1h-1 1zm-1 0 1 1-3-1h2zm-452 0h-1 1zm620 0h-1-1 2zm82-1 3 2h-2-1v-2zm-707 0v1l-6 1 3-2h3zm624 0zm-22-2 4 1h-1l-4-1h-2 3zm-98 1-2-1h1 2l-1 1zm99-1h-2 2zm-109 0zm110-1-4 1 1-1h3zm-1 0h-2 2zm-12 0h1-3 2zm-94-1v1h-1l1-1zm10 1h-2v-1l2 1zm78 0-2-1h1l1 1zm-85-1h-1 1zm-10 0h-1 1zm-31 0h2l-1 1h1-1l1 1h-1l1 1 6 4h-5l-3-1h-1 2l-2-1h1-1l-1-1-3 1-1-1v-1h2v-1h1l-2-1h2l-1-1h3 1zm30-1v1h2l-4 1v-1l2-1zm108 0 5 1-3 2h-1v-1h-1l-1-1 1-1zm-98 0 1 1-2-1h1zm100 0h-2 1 1zm-92 0h-1 1zm-9 0-1 1v-1h1zm93 0 3 1 1-1v1l2 1h1v1h1v1l-4-1-6-1 2-1h-2v-1h2zm33-1 4 1 1 1h-8l3-2zm-33 1-1-1h1v1zm-73-1h-1 1zm-5 0h-1 1zm-3 0h-1 1zm2 0h-2 1 1zm3 0h1-1zm96 0h2-3 1zm11 0h-3 1 2zm-49-1zm-109 0zm49 0h-1 1zm53 0h2l-1 1-3-1h2zm-52 0h1-1zm2 0h-1 1zm-14 0h1-1zm12-1h-1 1zm2 0h-1 1zm-9-1h-1 1zm-1 0h-1 1zm122 0h8l-3 1-7-1v-1l2 1zm-13-1h-1 1zm-9 1-1-1h1v1zm-144-1v-1 1zm42-1 1 1-1-1zm2 0h1-2 1zm-45 0h1-1zm71 0h-1 1zm81 0zm-149 0h-1 1zm146 0 4 1h1l-1-1h1-1l10 1-1 2-2-1h-2v-1h-1v1h2l3 1h-8v1l-6-2h1l-2-1h1l-2-1h3zm25 0h-1 1zm-107 0zm0 0h-1 1zm2 0h1-3 2zm1-1v1l-1-1h1zm0 1v-1 1zm30-1zm-35 0h-1 1zm33 0h1-2 1zm-31 0h-1 1zm-1 0h-1 1zm2 0h-1 1zm2 0zm95-1v1h-1l1-1zm-94 0h-1 1zm-55 0h1l1 1-12 3-2 1h1-2l1 1-1-1v1h-2l2 1h-1-2 2v1l-3-1 3 1h-2 1v1h-2-2-1l1-1h-3l4-1h-2l2-1h-2l3-1h-2 1l-2-1h3v-1h1-1l10-2 7-1zm37 0zm14 0h-2 2zm110 0h-1 1zm-121 0h-1v-1l1 1zm28-1h-1 1zm-30 0h-1 1zm31 0h-1 1zm2 0h-1 1zm-2 0h-1 1zm-44 0h-1 1zm16-1h1-1zm22 0 5 1-3 1h7l-1 1 8-1 4 1 1 1 3 1h-3l4 1-5 4h-2l-1 2 1-1 7-1h-3l1-1h2l2 1 1-1 3 2 1 1 2 1-2-1-1-1h1l-2-1h10 1v1l3 1h4l7 1 4 1v-1l3 2 2 1-2-1-2-2h1l4 2h1l-1-1 2 1h-1l7 3 1-2h-1v-2l3 2 4-1 6 2v-1h-1l-1-1 4 1-2-2h1-3l3-1-2-1 14 1-5 1h1 1l3-1v2l-1-1h1-1-1 1-1l1 1h2v-2l4 1 3 1h-2l5 1h-1 3l2 1h2 6 7l3 1 2 2 3 1 2 1h2v1h1-1v-1l-2-1 4-1 7 1v-1l4 2 6 1v-1l-3-1h-1v-1l-2-1 7 1h6l11 2 17 11-2 1h-2l-3-1h-1l3 1h-6 6l2 1h2l4 2h-1l1 1v-1l3 1v1l2 1 1 1-7-2 1 1-5 3 1 1-2 2 1 2-5-2h-4v2l-2-2-2 1h1v1l-2-1v1l-1-1v2l2 1h-1 1l1 3 2 1-1-1 2 1 3 3h1l2 2h-2v-1h-1 1l-1 1 1 2 4 2-3 2 1 2 2 1h-4 1l2 3-1 4-15-14-4-4v-2l-1-1 2-1 1-7 2-1-4-4 3-1h-4-2l3 3h-2l-1 3-1-2-1 1-1-1-1-2-1 1-2-1h-3l-1 6 1 1h3v1h-4l-3 1-1-1h2l-8-2-1 1 1 1-11-1-3 1-1 2v2l-4 9h2 3l2 3h1l-1-1v-1h1l-1 1h2v1l2-1v1l1 1-2-3h3l6 3 1 1h-2l4 2 2 5 2 2-1 1 3 4-2 15-3 3-3-1-1-1v1-1h-1v2h-2l1 1-1-1 1-1v-1l-4-5 1-1h4l-2-6 1-2-2-1h-3l-1 1h-4l-3-2v-1l-10-3v-1l-10-8-8-2h-5l-1 2h2l1 2v5l1 1-3 2-3-1-7-2-1 2-5 2-6-1-2-1-1-1-3-1h-10l-3-3-9-2-1 3 2 2-1 2-7-1-2-2h-5l-8 5h-1l-3-1v-1l-1 1-4-2h-1l-4-3-4 1-3-2v2l-9-8-5-2v-1l-4 3h-2l-1-1 1-1h-3l-2-1 1 1h-2l-1-1v-1l-2-2h-4l-7 2-1 1-4 1h-1-4l1 1h-1l2 1h-1l3 1h-3v1l1 1-2 1 5 2-1 2h-3l-1 1-3-2-2 1-3-1-1 2-3-2v2-1l-4-2h-2v-1l-3 1-1-1-4 3 1 2-2 1-2-2-1 2v1l1 1-1 2 2 1 1 1h2l3 3-1 1 1 1h-1l1 1h-1l-1 1-1-1v1l-1-1 1 1-2 3 3 3v-1 3l4 3-2 2-3-2-5-2h-2l-3-2-7-1-1 1-3-3-6-3h1l-1-1 3 1-1-1 2-2-2-1h2l-1-1 2-1h-2v-1l1-1h2v-2h-1l1-1-1-1h1v-2l-5-1-1 1-1-2h-5l-1-2-2-1-1-1h1l-2-2h-1l-4 1-2-3 4-1-3-1v-1l-3-2 1-1-1-2-7-1-1-3v-1h-1v-1l-1-3 1-2v-1l5-1-4-1v-1l-2 1 7-7-4-2 1-2-3-2v-2h1l-3-3 1-2-3-2-1-1h1l-1-1h1l2-2 2 1-1-1h5-2l2 1h1l-1 1 1-1 5 1 12 4 1 1v1l-2 2h-3l-15-3 5 2h-1l3 2h1-1v1l1 2 7 2h1l-1-1h-2l-1-2h1 3l-1 1h6l-3-2 4-3h3l1 1h-1 1l1 1v-3l-2-1v-3l-3-1 6 1h2v1l-2 1-1 1 4 1h2v-2h3l-2-1 7-2v1h1l-1-1 3-1h1-2l2 2h-2 4v-1h4l3-1 1 1v1h2l-1-1 2-1-2-1 1-1 7 1h2v1l5 1 3 1h1v-2h-2l-1-1-3-1v1-1-1l-1-1h-1v-1h-1l3-2-1-3h5 3l1 2-1 2 3 1 1 1-1 1 3 3 3 2h-1l1 2-2 1h1l-1 1 1 1h-2-1l1-1h-4l1 1 6 1 3-2-1-1 1-1v-2h-2v-1l4-1 2 1 1 2 5 1-4-1-1-1h1l-2-2h-4-4v-1l-2-1v-2l-4-2 1-1 2-1-2-2 3 1 1 3 4 1h4l-6-2-1-1 4 1-2-1 1-1 8 2h3l-1 1 1 1v1l1 1-1-1 1-1 1 2h1-1 1l1-1-3-2v-1l-8-2v-1l-2-1h1-1v-1h12l-2 1 3 1-3-1 3-2-4-1h2-3v-1l2 1v-1h-1 1l-1-1 11-2h-2 5v1l6-1h1v1l3 1-3-1v-1l-3-1h7-3v-1h-1l2-2h1zm-21 10h-1 1zm-81 23zm107-34h-2 2zm-25 0zm23 0h-1 1zm-27-2h-1 1zm17-1 1 2 1-1 5 1-9 1-1-1h2l-1-1h1-2l1-1h2zm-18 0zm-1 0h-1 1zm14 0h-1 1zm-15 0h-2 1 1zm-28 0h3-3zm42 0zm-15 0h-1 2-1zm-58-1h1-2 1zm62 0h-1 1zm-78 0zm3 0h1-3 2zm69 0h5l-2 1-3-1zm-55 0h-1 2-1zm-17 0zm1 0h-1 1zm82 0h1v1l1-1h3l1 2h-1 3l-4 1-6-1-1-1h-2-1l2-1h-1 1 1 3zm-64 0h-1 1zm-9 0-1-1h1v1zm-3-1h-3 3zm1 1v-1 1zm5-1v1h-2l2-1zm-8 0h-1 1zm5 0h-1 1zm1 0h-1 1zm-3 0h1-1zm6 0h3l-3 1-2-1h2zm-6 0h-1 1zm1 0h-1 1zm-3 0h-1 1zm9 0zm-1 0h-1 1zm-2-1v1l-3-1h3zm-16 0 2 1-2-1-1 1h1-2-1-2l5-1zm25 0-1 1h-4l5-1zm-12 0h1-3 2zm6 0h-2 2zm-15 0h3l-5 1 1 1h-1l-3-1h2-1 3v-1h-1 2zm51 0h1-3 2zm-37 0h-1 1zm-14 0h1-1zm10 0h4-4zm4 0zm-7 0h6-6zm11 0h-1 2-1zm-4 0h-1 1zm56 0h-3 3zm-44 0h-2-3 3 2zm-10 0-1-1 1 1zm-4 0h-1l2-1-1 1zm66-1 4 1-1 1h2-1l-3 1h-2-3l1-1h-3 2-1l1-1h-1l5-1zm-61 0h-1 1zm-5 0h1-5 4zm3 0h-2 2zm-9 0h1-1zm6 0h2-4 2zm1 0zm6 0h-1 1zm1-1 2 1h-3l1-1zm-6 0h-2 2z')
  static Path get ru => _$MapSvgData_ru;

  @SvgPath('m615 194 1 1-2-1h1zm0 0v-1 1zm-15-26zm-4-7zm37-1v1-1zm-40-1-1-1 1 1zm26-4-7-7-5-2-3-1-6 2 3 3-1 2h-2l-2 3-3-1-1 4h2l6 9 1 3 3 2 2 4 1 4 4 4 6 11 1-1 1-2h3l5 2v4l6-9 9-2 8-3 2-7-2-2-7-1-3-4-1-1h-1l-3-4v-2l-4-3-1-3h-2l-1-2h-3-5z')
  static Path get sa => _$MapSvgData_sa;

  @SvgPath('m593 395h-1 1zm0 0v-1 1zm-12-78h-2l-3 2-3 3-1 2-2 1-2 3-2 1-4-2-2 3-2 2h-3l1-3-2-3-1 11-2 2h-3l-2-1-1-2-1 2 4 10v2l-1 1 1 3v2l1-1v1l3 1 7-2h8v-1h2l4-2 6-6 4-6 3-2 2-6h-2v2l-3-1v-1-2l1-1h2v-4l-1-7-4-1zm-5 21 2 2-1 2-2 1-1 1-1-1-1-2 4-3z')

  static Path get ls => _$MapSvgData_ls;
  @SvgPath('m573 322 3-3 3-2v-1l-3-1-1-3h-1v-1l-3-2-2-6-3 1-2 2-1-2-6 1v12h-3v9l2 3-1 3h3l2-2 2-3 4 2 2-1 2-3 2-1 1-2z')
  static Path get bw => _$MapSvgData_bw;
  @SvgPath('m451 201 1 2 2-1-1 1v1l4-1 4 2h-2l-2-1v1h-2l-3 1v1h1 3v-1 1h-3v1l6-1h2l4 1h2v-2l-1-2-1-4-6-6-5 1-1 1-1 3-2 1 1 1z')


  static Path get sg => _$MapSvgData_sg;
  @SvgPath('m619 212-1 1v2l4 4 8 3h3l-8 10h-4l-5 2-2 4v12l1 2 6-7 7-6 5-6 8-16v-3h1-1l1-4-2-1-2 2-7 1-3 1h-2l-2 1h-2l-3-3v1z')
  static Path get so => _$MapSvgData_so;
  @SvgPath('m451 159h-1 1zm6 0v-1 1zm-4-1h-1 1zm2-1h-2l1 1 1-1zm5 1h1l1-2-2 2zm-9-1v-1 1zm11-2 1-1-1 1zm29-20zm-6-2zm6 0zm12-9zm0-1-1 1 1-1zm4-2v-1l-2 2h2l1-1h-1zm3-1h-1l1 1v-1zm-31-12-3 2v2h1l-1 1h1l-1 1h1l1-1v1h4l1 1-2 2v4h-1l1 2-1 2 1 1-1 1v2l2 1 1 2h2l2-1h6l2-2 2-1 1-3 2-1-2-2 1-2 2-3 6-3v-1l-4-1-5-1-4-1-13-1-2-1zm2 21z')

  static Path get lc => _$MapSvgData_lc;
  @SvgPath('m601 181m-16-4h-17v7h-3v1 12l-2 1v1l-2 2 1 1-2 1 1 2-1 2h1l2 5 2 4v3l2 1v-2l1-1v-2l3-1 1 2 2 1 2-1 1 1v1l2-1 1-1h1v-1l1-1 2 1 1 1 2-1 1-2 1-1v-2l-1-1h2v-1h2l-1 2 1 3 2 2v0c1-1 1-2 1-2h1l2-6h1l1-5v-3l1-3v-3h1l4-3-4-3-1-7-1-3h-7-8z')
  static Path get sd => _$MapSvgData_sd;
  @SvgPath('m590 208v1h-1v1 2 1h-1v1l-2 2h-1-1v-1h-1v-1h-1v1h-1v1h-1v1h-1c0 1-1 0-2 0h-1-1-1-1-1v-1-1h-1v-1h-1-1v1 1h-1v1 1h-1 1v1h-1v1 1h1l1 1h1-1 1v1h-1 1v1h1v1h1l1 1v1 1h1v1-1 1h1v1l1 1v1h1v1h1 1 1l1-1h1v1l3 3v-1 1h1v-1h1 1v1h1v-1h1 1 2l2-2v-1h1 1 1 1v-1-1h-1l-1-1v-1l-1-2h-1 1v-1h-1l-2-2h-1v-1h-1l1-1v-1h2v-1-1-1h-1 1v-1-1h-1v-1l-1-1v-1l-1-2v-1h1v-1z')
  static Path get ss => _$MapSvgData_ss;
  @SvgPath('m538 69v-2l-1 3 1-1zm3-1 1-3-2 2 1 1zm2-3h-1l1 1v-1zm-18 0h1-1zm12 0zm-11 0v-1l-1 1h1zm17-1zm-4-2zm2 0h-1 1zm0 0-1-1 1 1zm-2-1zm0 0h-1 1zm-1 0h-1 1zm3 0zm-1-3zm-3-5v-1 1zm1-1 1-1h-1v1zm6-3zm0-14-3-1h-1l1 1-1 1-4-1v2h-3l-1 1 1 2-2 1v1h-2l1 2-2 2 1 1v1h-2l-2 1v1l1 3v1l1 1-1 1 1 2v1l-2 1 1 2h-1-1v2h2v1 1l3 3-1 1h1-1l1 2v1h3v-1l1-1h3l1-4-1-2 1 1v-1 1-1l-1-1h1l-1-1h2l1-1v1l1-1v-1h1-1 1-1-1-1v1l-1-1h-2l3-1 1 1-1-1v1-1h1-1 1v1l3-1-5-3v-3l1 1-1-1 1-1-1-1h1l-1-1h2l2-2 3-1 1-1-1-1 1-1-1-1h2l-1-1 1 1v-1h1 3l-1-2v-1l-1-1v-1-1l-4-2z')


  static Path get tt => _$MapSvgData_tt;
  @SvgPath('m776 226zm1 0zm3-2zm-3 0zm-1-1h-1 1zm0 0h-1 1zm-2-1zm0 0v-1 1zm0 0-1-1v1h1zm0-1zm-1-3zm-1-1zm0 0zm5 0zm-4 0zm5 0zm-1 0zm-4-1zm5 0zm6-6zm0-1h-1 1zm-1-1zm-4-2zm0-1zm-5-22h-2l-1 1h-2l-1 4h-1l5 7h1l-2 4 3 5v2l2 4-2 4-1 7h1l5 6v-1l1 1h1l1 2 1-1 1 1v-2l-1-2-4-1-2-6h-1l-1-4 2-6v-3l2-1v3h4l2 3-2-6 2-2h6l1-1-1-4-2-2-1-3-2-3h-2l-2 2-1-2-3 3v-1-6h-2l-1-2h-1z')
  static Path get th => _$MapSvgData_th;
  @SvgPath('m695 129v-1h-1v-3h-3l-1-3-4 1-2-2-1 1h-5v-2h3l1-1h-2l1-1-1-1-2 1h-1l1 1-2 1h1l-1 2h-3l1 1h2l1 3-1 3 1 1 2-1h1v-1h2v-1l1-2 2 1v1h1v2l1 2 3-3 5 1z')

  static Path get tk => _$MapSvgData_tk;
  @SvgPath('m19 315zm-1-1h1-1zm1-3zm0-1zm1 0h-1 1zm-2 0h-1 1zm2 0v-1 1zm-2-1h-1 1zm2 0h-1 1zm-2-2zm1-1zm1 0h-1 1zm-1 0zm1 0h-1 1zm-7-9v-1 1z')
  static Path get to => _$MapSvgData_to;
  @SvgPath('m503 217v-2l-2-1 1-2h-3l1 3v2h1l-1 2 1 2-1 4 2 3 2-1-1-10z')
  static Path get tg => _$MapSvgData_tg;
  @SvgPath('m517 247 1-1h-1v1zm3-5z')
  static Path get st => _$MapSvgData_st;
  @SvgPath('m528 140h-1l1 1v-1zm0-3zm0 0h1-1zm-1-8zm-2 0h-1l-4 2 1 6-2 3 2 4 2 1 2 6 1-1 1-3 3-3v-2h-1l-1-1-2-1v-1l2-4-2-2 2-3-2 1v-2h-1zm-3-1z')
  static Path get tn => _$MapSvgData_tn;
  @SvgPath('m566 121zm0-1h-1 1zm4-2zm19-4h-5l-5 3-5-1v1l2 1h-3l1 1h-6l-2 3h3-1l1 2h-1l2 1h-2l-1-1v2l3 1v2h1l-1 1h3l-2 1 2-1-1 1 2-1 3 3 2-1v-2h1l5 3 3-1 2-2 2 1 1-1 1 1-1 1v1l2-1v-2l7 1 4-2h4l1-1 3 1 1 1 1-1-1-2h-1l1-2-2-3h1v-1h1l-2-1h-1l-1-2-1-1-1-2h-4l-3 2h-4l-6-1-1-1h-1l-1-1zm-20 0-3 1 1 1-1 2h2l-1 2 3-3h4l-3-2v-1h-2z')
  static Path get tr => _$MapSvgData_tr;

  @SvgPath('m637 124v-1 1zm12-12h-2l1 1-1-1-2 2v2h-3-1l-4-3h-3l-1 2 2 2-1-2v-1h2l1 2 2 1h-1l1 1h-2l-2-1v2l1 2v-1h1v1l1 1-1-1v2l1-1 1 2 1 5 2-1 1-2h5l1 1 5 1 3 3h2l1 3 3 2 1-1h1v-2l3-1v-3l3-1 2 1v-3l-7-3-6-3-2-3-5-1-1-3-3-1z')
  static Path get tm => _$MapSvgData_tm;
  @SvgPath('m610 272v-1 1zm-1-5v-2 2 1-1zm1-3v-1 1zm-22-14h-4l1 4v1l-1 1 1 1-2 4h-2l1 6 3 6 1 1 5 3 4 1 1 6 3-1 1 1h3l2-1 3-1 4-2-2-1-1-4v-4-2l-2-3 2-4-5-4v-1l-10-7h-6z')
  static Path get tz => _$MapSvgData_tz;

  @SvgPath('m484 90v-1 1zm12-3h-1 1zm1 0zm-9-1zm0-7zm1 0h-1l1 1v-1zm-5-6-2 1-2 2 1 1 1-1 2 1 2-1-1-1h1l-2-2zm1 0h-1 1zm2 0v-1 1zm-2-2-1 1h1v-1zm2 1v-1 1zm-2-1h-1 1zm0 1 1-1h-1v1zm1-1zm0 0v-1 1zm0-1zm-2 0zm2 0zm-1 0zm-2 0zm3 0-2-1 1 1h-1 2zm-2-1zm1 0v-1 1zm-1-1h1-1zm-2 0zm2 0zm1-1zm-3 0v1-1zm0 0h1-1zm3 0zm0-1-2 1 2 1h1l-1-1v-1zm0 1v-1 1zm-2-1h-1l1 1v-1zm0 0zm-4 0zm4 0v-1 1zm2-2h-2v1 1l2-2zm-1 1h-1v-1l1 1zm5 14-1 2 1-1v2l-3 2 5 1 2-1-1 2h-3l-4 3h1l2-1 2 1 2-2 1 1 3-1h1 1 2l2-2h-2l1-1h1l1-2-1-2h-3 1v-1l-3-1h2v-1l-3-2-1-3-2-1h-3l3-1h-2l2-1 2-2-1-1h-5 1l-1-1 3-1v-1h-4l-1 2v1h-1v1h1l-1 1h1l-2 1h1-1l1 1 1-1v1h-1v3l1-3v2l1-1h1-1v2l-1 1v1l1-1 1 1v-1h3l-2 2 1 1 1-1v2l1 1h-4zm12 6zm-9-22zm0 0h-1 1zm1-1h-2l1 1 1-1zm0 0 1-1h-1v1zm0-1h-1l1 1v-1zm3-3-1 1h1v1l1-2h-1zm1 0v-1l-1 1h1zm0-1z')
  static Path get gb => _$MapSvgData_gb;
  @SvgPath('m581 102v-1 1zm3-1zm-5 0-2-1 1 1h1zm0-19-4 1h-2v2l-12-2h-4l-1 2 1 2-3 4 1 1h-1l-1 2 2 2h5l4-1 3-1 4 1v2l3 3h-3v2l-1 1 3 1v-2 1l2-1 1-2h2v-2l1 2h1-3 2l-1 1h5v1l-3 1 3 1v2l2 1 3-3h2 1-3-1v-1l-1-2h-1l3 3h-1l-2-2-2-1h2v1-1h1l1-1-1 1 2-1 6-2v-1l1-1h2v-2h-1l1-1-1-1h1v-2l-5-1-1 1-1-2h-5l-1-2-2-1-1-1h1l-2-2h-1z')
  static Path get ua => _$MapSvgData_ua;
  @SvgPath('m292 184zm0 0h1-1zm-220 2 1-1-2-2-1 4 2-1zm-3-4zm-1-1zm1 0v1l2-1h-2zm-1-1h-1 2-1zm-9-2v1-1zm7 1v-1l-1 1v1l1-1zm-6-1v-1 1zm2 0 1-1h-2l1 1zm-6-4zm-7-2zm227-3zm-1 0zm1 0zm1 0zm0 0zm1 0zm-1 0zm1 0zm1-1zm0 0zm0 0zm-238 0zm239 0v-1h1l-1 1zm1-1v-1 1zm0-1zm-249-1zm245 0zm-251-1zm250-1zm0 0v-1 1zm0 0v-1 1zm0-1zm-41 2v-3 1 2zm40-4zm-40 1 2-2-2 2zm2-3v1-1zm1-1h1l-2 1 1-1zm1 0zm42 1v-2 1 1zm-27-3zm0 0h1-1zm2 0zm1-1h-1 1zm0 0zm0 0zm-14 1 1-1-1 1zm16-1zm-1 0v-1 1zm1 0v-1 1zm-7 0v-1h-1l1 1zm18-1zm-12 0zm12 0h1-1zm-12 0zm1 0zm12 0h1-1zm-11 0v-1 1zm-1-1v-1 1zm3-1h-1 1zm-1 0zm-1 0zm2 0zm5 0h-1-1 2zm-83-8v-1 1zm104 0v-1 1zm-106-1h-1 1zm3 0-1-1 1 1zm107-2zm-111-1h-1l1 1v-1zm-1 0zm2 0zm115-2h-1 1zm-1 0h-1 1zm1 0 1-1-1 1zm1-1zm0 0h1-1zm2-1v-1 1zm0-1v-1 1zm0-5h1-1zm1 0zm0-1zm-119-2zm121 1 1-1-1 1zm4-5h-1 1zm0 0v-1h1l-1 1zm1-3 1-1h-1v1zm3-1h1-1zm3-1-5 1h1l5-1h-2 1zm6-1h-1 1zm-2 0zm-2 0 1-1-1 1zm9-8 1-1h-1v1zm1-1zm1 0v-1 1zm-136-7zm4-2zm1-1h1-1zm2-3-1 1 1 1v-2zm-1 0h1-1zm0 0zm1-1-1 1 1-1zm0-1zm69-1-1 1h-67v2h-1v1l-1 2-2 1h-1l1-1v1l1-1v1l1-2-2 1 2-1v-1l-4-1-2 5h1-1 1l-2 2h2-2l-5 9-2 1-2 5-2 3v2l-1 2 1 2v1l1 1v-1h1 1 1-2l-1 1v1-1l-1 1 1 2-1 2 2 4-1 2 5 1 2 2v3l7-1-1 1 4 2 5 2h7l1-2h5l3 4v3l2 2 4-3 2 1 4 7v3l5 2-1-3 1-1-1-1h1v-1h1l2-2h1l-1-1h2l-1 1 4-2v-1h1l-1-1h2l-1 1 3-1v-1 1h4 1l1 1 3 1v-2l2 2h1v-1h-1v-1l1-1h-1v-1h-2 6l1-1v1l5-1 2 2h-1v1l4-1h1l2 4-1 3v-1l1 1-1 1 1 2h1l-1 1 1-1v1 2h1l1 2h-1 2l2-5-1-5v-1 3l1-2-1-4v-5l3-3v-1 1-1l4-2v-1h1v1l1-2h2l1-1-1 1 2-3h3l-1-1 1-1-1-1 2 1 2-1v-1l-1 1v-1h-2l1-1v1l1-1 1 1v-2 3-4l-3-1h3l-1-1v1l1-1-2-2 2 2v-1l-2-2 1-1-1 1 2 1v-1l1-3 2-1-2 3h1l-1 1h1l-1 2h1l-2 2 4-4v-4l1-1-1 1 1 2v1l1-2h1v-1l1-1v1l1-2h-1l2-3-1 2 3-2h4l1-2v1 1l3-1v-1 1h-1l-1-2 2-1v-2l2-2 1 1v-1h2l1-2v1-1h1 1v1l1-1h1l1-1v-1l-1-1 1-5-3-1-7 7-10 1-6 4h-5-1l-1 2-10 4-1-1 2-2 2-3 1-5-2-2 1-1h-1v-1h-1v-1l-7-5-3 1h-3-2l-3-2h-4v-2zm-70 6v-1 1zm-8 6zm104 36zm-225-53v-1 1zm0-1h1-1zm861-1h-1l3 1-2-1zm-859 0zm857 0h-1 1zm-850 0zm-1 0h1-1zm-4 0zm4-1-2 1h2v-1zm1 1v-1 1zm-3 0 1-1-2 1h1zm4-1zm0 0zm849 0h-1 1zm3 0h-1 1zm-6 0zm-847 0h1-1zm7 0h-1 2-1zm835-1zm-832 0zm-4 0-4 1 4-1zm831 0h-1 1zm-824 0h1-1zm2-1h1-1zm822 0h-1 1zm-820 0zm1 0v-1 1zm0 0v-1h-1l1 1zm816-1h-2l2 1v-1zm-815 0zm4-1h2-1l-4 2 3-2zm6-1h1-1zm0 0-1-1v1l-4 1h3l3-2-1 1zm4-1h-1 1zm1 0h-1 1zm-2 0v-1l-1 1h1zm0 0 1-1h-1v1zm8-1h1-1zm0 0zm71-1 1-1h-1v1zm-62-1h1-1zm-9 0h-3l-2 1h5-1l1-1zm2 1 1-1h-1v1zm73 0v-1h-1l1 1zm-66-1h1-1zm-5 0h1-1zm68 0zm3-1-1 1h1v-1zm-4 1v-1 2-1zm-62 0 1-1-1 1zm-4-1zm4 1v-1l-2 1h2zm62-1zm-63 0zm0 0-2 1h2v-1zm62 0h1-1zm4 1 1-1-1 1zm-72-1zm7 0zm62 0zm0 0v-1 1zm1 0v-1l-1 1h1zm0-1h1-1zm1 0v-1 1zm-60 0h1v-1l-1 1zm57 0 1-1h-1v1zm-49 0v-1 1zm50 0 1-1h-1v1zm7 0v-1l-2 1-1 1 2-1-2 1h1l2-1zm-1-1zm-5 0h1-1zm0-1zm2 0h-1l1 1h-1l-1 1h1l-1 1h-1 2v2l1-1h-1l1-1v-1 1-3zm2 1v-1l-1 1h1zm-1-1h1-1zm1 0zm1 0h-1v1l1-1zm-50 0v-1l-1 1h1zm-7-1h-1v1l1-1zm57 0zm-51 0-1 1 1-1zm0 1 1-1h-1v1zm-35-1zm85 1v-1 1zm-2-1h-1l-2 2 1-1v1-1h1v-1h1-1 1zm2-1h-2v1 1h1v-1h1v-1zm-46 0h1-1zm-38 0v-1l-1 1h1zm78 0h1v-1l-1 1zm3-1h-2l-1 4 3-4zm0 0zm-40-1v-1h-1l1 1zm2 0v-1 1zm-2-1v1h-1-1-1v1-1l-3 1v2l1-1h-1 2l-2 1 2-1h1l1-1h2-1l2-1-1-1zm0 0h1-1zm38 1 1-1h-1v1zm3-2-2 1v1h-1v1h1v-1l1 1 1-1h-1v-1l1 1v-1h1-2 1v-1zm-37 1v-1 1zm40-1h-1l1 1v-1zm-41 0-2 1 3-1h-1zm40 1v-1l-2 2 1 1h-1-1v1l2-1 1-2v1l1-1h-1zm-40-1 1-1-1 1zm-19 0 2-1h-2v1zm21-1v-1 1zm5-2zm-6 0zm7 0zm14-1 2-1-1 1h-1zm-7 0v-1l-1 1h1zm0-1h1-1zm0 0h1-1zm1 0h-1 1zm-1 1 4-2h-1l-3 2zm2-2-1 1 1-1zm-1 1v-1 1zm-40-1-4 1 2 1h2l1-2h-1zm44 0h-1 2-1zm3 0h-1 1zm-61 0h-1 1zm46 0zm12 0h2-2zm-1-1h-1 1zm-2 0h1-1zm0 0zm-37 0zm40 0h1-1zm0 0h-1 1zm-2 0h-1 1zm-41-7zm0 0zm0 0zm0 0zm0 0zm18-1h1-1zm-19 0v-1l-1 1v1l2-1v1 1l4-1-2-1h-3zm20-7h1-1zm4-1h1-2 1zm36-13-5 1h-3l-2 1h2l-2 1v-1h1l-7 1h1l-8 3h-6l-2 2h-1l3 2-1 1h3l-1 1h2-2v1h3-1-2v-1h-1v1l-1 1h2-6-1l3-1h-2l-11 2 3 1-2 1-1 1 6 1 1-1-1 1 6-2v1l-3 1v1l-2 1h-2l-4 2-2-1-1 1h-2 1l-1 1h-2l-5 3v1l1-1h1 1-1l-2 2h4-1l-1 1v-1l-2 1 2-1h-1l-3 1 1 2 4-1 1-1 3-1-3 2-1 2-2 1h1l-3 1 5-1 1 2 4-2v1l-1-1v1l5-1-5 2 1 1h-2l-1 1v1h-1l-3 1-1 1-6 1-1 1v1l-3-1-6 3h-1v1l1-1h1l1-1-1 1h2l2-2h1l-1 1 5-1 4-1 1-1h-1 1l6-2 2-1 7-2 2-1 3-1-2-1 7-3v-1h1l4-1 7-2-3 1 2 1-3-1-3 1-1 1-3 2h2l-4 1 2 1 2-1 1-1-1 1 1-1h1l1-1v1l1-1v1l2-1h1 1-1l2-1h-2 2l-1-1 4-1-2 1 2-1-1 1 3-1-1 1h1-2 2-1 2l-2 1h1l3-1-2 1 1 1h6v1h1l3-1 1 1-2 1 1-2-2 2 2 1 1 3 2-1v-1h-1l1-1 1 1 1-1v1h-1v1h1l3-3-2 3h1v1l2-1-2 1 1 1v-1 2l-1-1v1l-1 1h1v3l-2 1v1l1-1 3-1-3 3 2-1-2 1v1l3-2 1-1v-1l-2-2 1-5v-4l-7 3v-1l-1-3 1-1h-4l29-26-3-1-4 1-6-2h-6 1-2 1l1-1h-4l-1-1-3 2v-1l2-1h-1zm2 36z')
  static Path get us => _$MapSvgData_us;
  @SvgPath('m491 212h6 2 3l1-1h2l1-2-1-2h-2l-1-1v-1h1l-2-1-1-4-3-1-3 2-1 1h-1l-1 2h-1l-1 1-1-1-1 1v3l-3 2v4l2 2h3l3 1-1-5z')
  static Path get bf => _$MapSvgData_bf;

  @SvgPath('m666 123 7 3v3h3l1-3-1-3h-2l-1-1h3l1-2h-1l2-1-1-1h1l2-1 1 1-1 1h2l2 1 3-2-3-1-2-2v2l-3-2 2-2h-1l-4 3-1 2-2-2h-3l-1-2h-1l-1-3h-1l-3-2h-7l-11-6-6 2 3 11h3v-2l2-2 1 1-1-1h2l3 1 1 3 5 1 2 3 6 3z')
  static Path get uz => _$MapSvgData_uz;

  @SvgPath('m329 220h-1 1zm0 0h-1 1zm0 0v-1l-1 1h1zm0 0v-1 1zm0-1-1 1 1-1zm0 0v-1 1zm0-1zm1 0h-1 1zm-5-2h1-1zm-1-2zm-3-1zm-4 0v-1 1zm4-1h-1 1zm-7-2zm1 0zm4 0v-1 1zm-14 0-1-2v2h1v1l-5 1 1 5-2 1-1-2 2-3-1-3h2-2l-2 2-3 6h2l1 3v2l1 2h5l3 3 5-1-2 6 2 3-2 2 2 1 1 4 1 2 2-1 1 1 6-5v-1h-2l-1-3-1-2 2 1h2l1 1 1-2 4-1 2-2h-1l-2-3 1-2 3-1-1-2 2-2v-1h-3-2 1l1-1v-1h1l-1-1-2-1v1l-1-2v2-1h-1v-1h-1-1 2l-1-1v1-1l3-1h-7l2 1-4 1-3-2-6 1v-2l-4-2zm23 7v-1 1z')
  static Path get ve => _$MapSvgData_ve;
  
  @SvgPath('m796 220 1-1-1 1zm-8-2h-1 1zm7-1v-1 1zm1-1zm0-1zm0 0v-1 1zm-7-1h-1l1 1v-1zm8 0h1-1zm0 0zm5 0zm-5 0v-1 1zm0 0v-1h-1v1h1zm6-6zm0 0zm-5-13h-1 1zm-3-12zm-1-2zm-1 0zm-1 0zm0 0zm1 0zm1 0zm-2 0zm2 0zm-2 0zm3 0v-1 1zm-1 0v-1 1zm1-1v1-1zm-1 1v-1 1zm0-1zm0 0zm1 0v-1l-1 1h1zm0-1zm-7-5-1-1-3 3-2-1-1 1-2-1v1l2 2 1 3 2 1 1-1 3 3-1 1h-2v1l4 2v1l4 4 1 2 2 1v1l1 2v1 1l1 7-3 1v1h-1l1 3-3-1-2 2 2 2-1 4h1l3-3-1-2 2 2-1-2 1 1-1-1h2l-2-1 1 1v-1-1h1v1-1l1 1 2-1 2-2 1-2-1-1 1-2-3-8-6-6-4-5v-3l2-3v-1l1 1 1-1v-1h1l-4-2v-2l-3-1z')
  static Path get vn => _$MapSvgData_vn;
 
  @SvgPath('m563 303-6 1h-6l-1-2h-12l-3-1-4 1 1 2 7 15-1 4 2 11 4 4 1-2 1 2 2 1h3l2-2 1-11v-9h3v-12l6-1 1 2 2-2 3-1h-1-4-1z')
  static Path get na => _$MapSvgData_na;



  @SvgPath('m647 209zm0 0v-1l-1 1h1zm-3-1 1 1-1-1zm-24-1zm28 0h-1l1 1 2-1h-2zm-30-3v-1l-1 1h1zm0-1v-1l-1 1h1zm-1-4v-1 1zm27-2v-1l2-2-4-7-9 2-6 9v-4l-5-2h-3l-1 2-1 1v4l2 6 1 2h1 3l2-2 8-2 1-2 9-4z')
  static Path get ye => _$MapSvgData_ye;
  @SvgPath('m568 303h1l5 1 2-3 3-1v-2l4-1v-2l8-4-1-1 1-3 1-1v-4l1-1-2-4-5-3-1-1-5 1-2 2 1 5-1 3 3 3 1-1v4l-1-1-1 1-2-3-2-1-1-2-1 1h-3l-1-1v-1h-3l-1-1v6h-6v10l4 5h4z')
  static Path get zm => _$MapSvgData_zm;
  @SvgPath('m590 302v-2l-4-2h-3v-1l-4 1v2l-3 1-2 3-5-1 2 6 3 2v1h1l1 3 3 1v1h2l4 1 3-3v-3l2-2-1-3 1-2v-3z')
  static Path get zw => _$MapSvgData_zw;
  @SvgPath('m838 281 1-1-1 1zm4 0 1-1-2 1h1zm1-1v-1 1zm-9-3h-3l3 2h1 1l-2-2zm14-1-1-1-1 1v1h-1l-2 3 3-1 2-2v-1zm-27-1zm12 0v-1 1zm9-1h1-1zm-10 0v1l1-1h-1zm-17 0zm31-1-1 1 1-1zm-2 0h1l-2 1 1-1zm-1 1v-1l-1 1h1zm14-1h-1 1zm-33 2 1-1-1-1-2 2h2zm31-2h1-1zm4 0h-1 1zm-32 1v-1 1zm59 0v-1 1zm-57 0-1-1 1 1-1 1-2-1-1 2 4-1v-1 1h2v-2l-1 1v-1l-1 1zm13-1-2 1-5-1-2 2h6l3-1v-1zm5 0-1 1 2-1h-1zm2 0h1-1zm-18 0zm-10 0h-3l2 2 2-1-1-1zm35 0h1-1zm8 1 1-1-1 1zm-10-1v-1 1zm7 0v-1 1zm-8-2h-2l-1 1 3-1zm12 1v-1 1zm-10-1zm32 0h-2l-2 3h2l2-3zm-23 0h1-1zm1 0zm-26 0v-1 1zm-2 0v-1 1zm28-1zm-28 0h-1 1zm30 0-2 1v1l2-2zm0 0zm-49 0v-1 1zm40 0v-1 1zm-22-1v1-1zm-19 0h-3l2 1 1-1zm4 0h1-1zm65-1 1 1-1-1zm-19 0-1 1 1-1zm8 1v-1 1zm-82-1zm82 0zm0-1h-1l1 1v-1zm1 1v-1 1zm-2 1 1-1-1-1-1 2h1zm0-3v1-1zm1 0h-1l1 1v-1zm-79 0-3 3h3v1l5 2 3-1 7 3 4-1 4 2-1-1v-2l-4-1-1-2-4-1v-1l-2 2h-4l-1-2-6-1zm50 0h-1 1zm-10 2v-2-1 3zm-44-2v-1 1zm22 0v-1 1zm56 0v-1 1zm0-1zm-1 0zm-24 0v-1 1zm30-1-1 1v1h1v-2zm-90 0h-1 1zm85 2 1-1v-1l-1 2zm-25-2h-1 1zm-5 0v-1l-1 1h1zm-17-2zm44 0zm-25 0h-1v2h1v-2zm20 0v-1 1zm0-1zm-19 1v-1l-1 4 1-1v-1-1zm24-1zm6 0-1-1 1 1zm-7-1zm-23 0v1-1zm-61 0zm84 0zm1 0h-1 1zm7 0zm-19-1zm4 0zm-16 0v-1 1zm15 0 1-1h-1v1zm-17-1zm17 1v-1l-1 1h1zm1-1v1-1zm11 0zm-28 0h-1 1zm-17 1v-1h-1l1 1zm31-1v-1 1zm-32 2v-3l-1 2 1 1zm32-3zm-76 0zm74 0h-2v1l1 1h2l-1-2zm3 0v-1 1zm-59 0v-1 1zm64-1h-4l-1 2 1-1v1l2-1 2 1 3 1-1-2-2-1zm-62 0h-1 1zm-1 0h-1 1zm-19 1-1-1 1 2v-1zm87-2zm-66 0-1 2h1l1-1-1-1zm-22 1v-1 1zm94-1zm-95 0v-1 1zm98 0v-1l-1 1h1zm-32-1zm0 0h1-1zm-66 1v-1 1zm68-1v-1 1zm29 0v-1 1zm-30 0v-1 1zm7 1-1-2v1l1 1zm-5-2zm-2 0zm0 0zm0 0zm0 0zm0 0zm1 0h-1 1zm-1 0zm-1 0zm6 0h3-3zm-73 0zm66 0h1-1zm32 0h-1 1zm-26 0h-2 3-1zm-46 0v-1 1zm60 1 1-2-2 1 1 1zm-19-1v-1 1zm36-1h-2l4 1-2-1zm-76 0zm-8 1-1-1-2 2h2l1 2 2 1-1-1 1-1h-1l-1-2zm81-1h1-1zm-20 0h-1 2-1zm-2 0 1-1-1 1zm-50-1h-1 1zm39 0h-1v1h2l-1-1zm12 0zm-51 0zm58 0h-1 1zm-57-1h-1v1h1v-1zm-30 0-1 1 2 2-1-3zm101 1v-1h-1l1 1zm-11-1h-1l1 1v-1zm-1 0h-1 1zm13-1 1 2h2l-3-2zm-23 1v-1 1zm-80-1zm90 0v-1l-1 1h1zm-25 0v-1 1zm-49-1zm81 1-3-1-3 2v2h2l1 2h5l-1 2-1-1-2 1h-2l3 2v2l1-1 1-3v2l1 2h2l9 4 1 4 2 1h-2l2 1h-2l1 1v2l3-1v1l2 3 2-21-9-3-2 1v1h-2l-3 4h-1l-1-3-1 1-1-2v-4l-2-1zm1 9zm-81-9v-1h-1l1 1zm49-1h-1 1zm15 0-1 1 2 1-1-2zm-65 0h1-1zm50 0zm14 1v-1 1zm-80 0v-1l-1 1h1zm14-1zm53 0h-1 1zm-1 0zm-52 0zm3 0h-1 2-1zm47 0v-1 1zm-66-1zm20 0h-1 1zm72 0-1 1h1v-1l1 1-1-1zm0 1zm-90 0v-1h-1l1 1zm87 0-1-1 1 1zm-6-1zm-80 0h-1 1zm15 0zm1 0v-1 1zm-1-1zm65 0h-1 1zm-68-1h-1 1zm3 0zm-2 0zm-1 0zm68 0zm-65 0zm1 0zm-4 0zm1 0v-1 1zm66 0 1-1h-1v1zm-66 0v-1 1zm1 0v-1 1zm0 0v-1h-1l1 1zm-1 0-1-1 1 1zm0-1h-1 1zm2 0h-1l1 1v-1zm-5 0 1 1-1-1zm4 0zm2 0zm8 0zm-13 0h-1 2-1zm50 0zm-1 0zm-44 0h-1 1zm-3 0h-1 1zm1 0v-1 1zm-4 0v-1h-1l1 1zm-14-1h-1l2 2v-1l-1-1zm77 0 1-1-1 1zm-63-1h-2l2 1v-1zm-11 0h-1 1zm74 1v-1l-3 2-8-1-1 2-1-1-1 1-1 2 1 3-1-1v1l-1 3-2 3 1 2h1v7h3l-1-9 2-1 1 1-1 2 2 2v2h1v-1h3l-2-3 1-1-4-4h2l3-3h1 1l-1-1-4 1h-1l-1 2-3-3 1-3h11l2-3zm-78-2v-1 1zm12 1v-2 1 1zm-14-1 1-1h-1v1zm0-1zm88 1v-1l-1 4v4l2 2-1-4h3l-2-1 1-1v-2l-1 1-1 2v-4zm-86 0v-1 1zm86-1zm-26 0zm19 0zm8 0v-1 1zm-54-1zm55 2v-2l-1 1 1 1zm-9-2v-1 1zm-54-1zm-27 2-2-2 2 2zm35-2h-1 1zm-4 0h1v-1l-1 1zm-5 0v-1 1zm1-1zm32 0v-1l-1 1h1zm-32 0v-1 1zm32 0v-1 1zm0-1h-1 1zm-1 0h1-1zm2 0h-1 1zm22 0-1-1 1 1zm-22-1h-1 1zm25 0h-1 1zm-25 0h-1 1zm24 0v-1 1zm-25-1zm1 0h-1 1zm-27 2v-1-1 2zm22-2h-1l-1 3-1 1v1l-1 1v3h-3l-3-1-2 2h-4l-2-2v-1l-2 1v1h1l-1 2v4h2l-1 1h1l1 1v2 4h4l1-1v2l3-1 2 1h2l1 2 3-2 2-4-1-1 2-2v-1 1l2-1v-3l1-2v-1h1 2l-3-3v-1l-2-3 1-1h-2l3-1-2-1h1-4zm3 16zm27-16-1-1 1 1zm-4-2zm-49 0zm-34-3h-2l1 3 4 3 3 5 2 2 2 4 1 1 2 3 3 7 8 9v-1h2v-1l1 2 2-9-1-2-3-1-1 2 2-2-2-1v-3l-3-1 1-1h-1 1-1l2-1-2-2-2 2 2-1-1-1h-1l-4-5h-1l1 1-3-2v-1l-5-4-2-4-4 1-1-2zm24 21zm-4-4zm-22-17zm0 0zm0 0h1l-1-1v1z')
  static Path get id => _$MapSvgData_id;




  @SvgPath('m646 170-1 1 1-1zm-2 0h1-1zm0 0zm3 0zm-4 0h-1 1zm2 0zm3 0-1-1v1h1zm0-1zm-1 0zm3-3zm2 0-1-2v2l-4 4v1h-6l-1-1 3 4 7 1 1-4h1l-1-2 1-1v1l1-1-1-2z')
  static Path get ae => _$MapSvgData_ae;


 
  @SvgPath('m969 229zm0 0zm10-1zm1-1zm-1-2v-1 1zm-9-1zm0 0v-1 1zm1-1zm-2 0v-1 1zm-2-4zm8 0zm-14-1zm3 0v-1 1zm10-1zm-1 0zm-1-3zm-8-2h-1 1zm8-1zm-21 0zm14 0zm-5-1zm-1 0zm9-9z')
  static Path get mh => _$MapSvgData_mh;



  @SvgPath('m456 179h7v-4l3-2v-8h9l1-4v-2h-13l-1 3-2 2-2 5-3 3 1-1v1l-1 1-1 3-1 1v1l-1 3 1-2h3z')
  static Path get eh => _$MapSvgData_eh;
  @SvgPath('m550 112 2 2 4-1v-2l1-1-1-2v-1-1h-1l-2-1v-1l-4-3-3 1 1 2h1l-1 1h1l-1 2 2 1h-1v1l3 2-1 1z')
  static Path get rs => _$MapSvgData_rs;

  @SvgPath('m483 29v-1l-2 1h2zm52-9h1-1zm10-5h1-1zm-4-2zm0-2h-4l1 1-1 1h3l4-1-4-1h1zm-4-1h-2l1 1h3l-1-1h-1zm10 0h-1 1zm-28 0-1-1 3 2-2-1zm31-1h-1 3-2zm2 0h1-1zm-15 0h-1 1zm-1 0h-1-1 2zm-2 0v-1 1zm0-1zm-16 0zm0 0v-1 1zm10-1h-1-1l2 2-4-2-1 1 1 1-2-1h1-2l2-1-5 1 1 1h1 1-2l4 1h-2l1 1h3v-1h1 1 2-1l2 1h-7l1 1h6-4l2 1h-4 1l4 1h-2l3 1h1l-1-1 1-1h1v-2h1v-1h4l-5-1-1-1h-1 1l-3-1zm19 0h1-1zm16 0h-1 1zm-8 0h2-3 1zm-24-1h-1 1zm10 0zm-8 0v1l-1-1 1 1h-3 2-1l1 1 6-1-5 1 7 1 6-2-4-1-3 1v-1h-1v1h-1v-1h-4zm8 0h-1 1zm-8 0h1-1zm2 0h-1 1zm1 0h-1 1zm-1-1v1l-1-1h1z')
  static Path get sj => _$MapSvgData_sj;


  @SvgPath('m438 433h-1 1zm1-1zm0-2h1-1zm0-2zm-1-2zm-1-1h-1 1zm-2-1zm-23-5v-1 1zm-1-2h-2l6 3v-1l-1-1-3-1z')
  static Path get gs => _$MapSvgData_gs;

}
